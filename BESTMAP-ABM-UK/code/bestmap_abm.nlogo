extensions
[
  gis
  csv
  matrix
  pathdir
]

;-----------------------------------------------------------------------------------;
; Include external files
;-----------------------------------------------------------------------------------;
__includes
[
  "setup_fields.nls"
  "setup_farmers.nls"
  "setup_WTA.nls"
  "update_world.nls"
  "check_openness.nls"
  "social_network.nls"
  "select_fields.nls"
  "deliberation.nls"
  "helper.nls"
]

;-----------------------------------------------------------------------------------;
; Global variables, variables that are commented are defined via the interface
;-----------------------------------------------------------------------------------;
globals
[
  ;-----------------------------------------------------------------------------------;
  ; Model settings
  ;-----------------------------------------------------------------------------------;
  ; i-g-set-seed?                     ; if true, seed is set manually via interface, for nlrx set-seed? false
  ; i-g-model-seed                    ; manually set seed when set-seed? true

  ; i-g-start-with-existing-aes?      ; set the farmers' initial status. If true, the farmers start with existing AES at the beginning of a simulation
  ; i-how-integrate-regression        ; The way to integrate regression analysis results. "no" means not integrate the regression analysis results;
                                      ; "regression-yes-no-adoption" means to integrate yes-no regression model output, which is used to decide WTA to be above mean WTA and below mean WTA;
                                      ; "regression-sorted-score" means to decide WTAs of farmers based on sorted regression model output.
  ; i-g-years                         ; the number of years simulated
  ;-------------------;
  ; Model output
  ;-------------------;
  ; i-save-csv?                       ; save output files or not - agent status are recorded tick by tick
  ; i-save-stats?                     ; save the model stats or not at the end of a run.

  ;--------------------------------------
  ; For running the model using R nlrx
  ;---------------------------------------
  ; i-nlrx-run?                       ; whehter the model is run by nlrx. This decides how the seeds are set up and changes stats-output file names (if i-save-stats? is true).
  ; nlrx_id                           ; transfer the nlrx experiment name, random seed and runnumber (siminputrow) between R and NetLogo
  ; i-nlrx-run?                       ; controls the print-out messages of the model. If true, the simulation skips the print-out messages;
                                      ; if false, the simulation print out messages during the run-time
  ;-----------------------------
  ; Model testing
  ;--------------------------
  ; i-test-WTA-mode?                  ; if true, enter into the WTA distribution mode
  ; base-WTA-bs                       ; mean value of WTA distribution for buffer strips
  ; base-WTA-cc                       ; mean value of WTA distribution for cover crops
  ; base-WTA-gm                       ; mean value of WTA distribution for grassland management
  ; base-WTA-cnv                      ; mean value of WTA distribution for land conversion
  ; i-sd-mean-fraction                     ; the standard diviation of WTA distribution - the ratio of the mean value

  ;---------------------------
  ; For multiple runs
  ;---------------------------
  ; i-multi-run-AES                   ; For go-x-times function: to set which schemes to collect data about. The values of this variable can be: "All AES in list",
                                      ; "Buffer Strips", "Cover Crops", "Grassland Management" or "Land Conversion"
  ; i-go-x-times?                     ; set this controller for recording the seeds when use run-x-times function
  p-g-record-seeds                    ; record seeds in multiple run setting
  p-g-record-a-seed                   ; record seed for output purpose in a single run setting
  p-g-record-errors                   ; record prediction errors
  p-g-adoption-rates                  ; record adoption rates


  ;-----------------------------------------------------------------------------------;
  ; GIS input
  ;-----------------------------------------------------------------------------------;
  p-g-landscape-dataset-shp       ; shp input file (field-file.shp) with GIS dataset
  p-g-village-dataset-shp         ;shp input file with GIS dataset

  ;-----------------------------------------------------------------------------------;
  ; AES input
  ;-----------------------------------------------------------------------------------;
  ; lists to combine contract details for all AES as defined via interface/input
  p-g-aes-type-list               ; list with names of all AES
  p-g-duration-list               ; list with contract duration of all AES
  p-g-bureaucracy-list            ; list with administrativ effort of all AES
  p-g-offered-payment-list        ; list with offered payment levels for all AES
  p-g-area-min-list               ; list with minimum plot size for all AES

  ; individual variables for AES (defined via interface/input)
  ; i-g-area-min-buffer-strips    ; minimum plot size for buffer strips
  ; i-g-area-min-catch-crops      ; minimum plot size for catch crops
  ; i-g-area-min-grassland        ; minimum plot size for maintaining grassland
  ; i-g-area-min-conversion       ; minimum plot size for conversion of arable land to grassland

  ; i-g-duration-buffer-strips    ; contract duration buffer strips
  ; i-g-duration-catch-crops      ; contract duration catch crops
  ; i-g-duration-grassland        ; contract duration maintaining grassland
  ; i-g-duration-conversion       ; contract duration conversion of arable land to grassland

  ; i-g-bureaucracy-buffer-strips ; contract details bureaucracy buffer strips
  ; i-g-bureaucracy-catch-crops   ; contract details bureaucracy catch crops
  ; i-g-bureaucracy-grassland     ; contract details bureaucracy maintaining grassland
  ; i-g-bureaucracy-conversion    ; contract details bureaucracy conversion of arable land to grassland

  ; i-g-payment-buffer-strips     ; offered payment buffer strips
  ; i-g-payment-catch-crops       ; offered payment catch crops
  ; i-g-payment-grassland         ; offered payment maintaining grassland
  ; i-g-payment-conversion        ; offered payment conversion of arable land to grassland

  ; i-g-advisory-BS?              ; buffer stirps AES offers free adviosry service or not
  ; i-g-advisory-CC?              ; cover crops AES offers free adviosry service or not
  ; i-g-advisory-MG?              ; management grassland AES offers free adviosry service or not
  ; i-g-advisory-CVN?             ; land conversion AES offers free adviosry service or not

  ; i-g-site-selection            ; order of site selection for accepted schemes

  ;-----------------------------------------------------------------------------------;
  ; Discrete choice experiment input
  ;-----------------------------------------------------------------------------------;
  p-g-WTA-list                    ; list of lists for WTA for different AES
  p-g-sd-mean-fraction            ; the fraction - SD/mean for WTA normal distribution
  p-g-area-list                   ; list of lists with area on which farmers would apply individual AES
  p-g-advisory-list               ; list of advisory service (True (1)/Fasle (0)) of the four AES
  ;-----------------------------------------------------------------------------------;
  ; Farmers' attitude
  ;-----------------------------------------------------------------------------------;
  ; i-g-access-to-advisory        ; probability that a farmer has access to advisory
  ; i-g-social-network-type       ; choose which other farmers have an influence (spatial neighbors, FSA)
  ; i-g-social-network-radius     ; radius in which other farmers are considered if influence is from spatial neighbors
  ; i-g-prob-open-experience      ; probability that farmer with prior experience is open
  ; i-g-prob-open-advisory        ; probability that farmer with access to advisory is open
  ; i-g-prob-open-social          ; probability that farmer with positive social influence is open
  ; i-lambda-openness             ; Assume the intrinsic openness in a population is proportional historic adoption rates
  p-g-prob-intrinsic-open-list    ; probability that farmer is intrinsically open (list of all FSAs and AES)

  ;----------------------------
  ; WTA variables
  ;----------------------------
  p-g-base-WTA                   ; row 1: baseline WTA (4 values)
  p-g-one-year-WTA               ; row 2: difference for one year (4 values)
  p-g-ten-year-WTA               ; row 3: difference for ten years (4 values)
  p-g-free-advisory              ; row 4: additional value for free advisory available (1 value)
  p-g-low-bureaucracy            ; row 5: additional value for low bureaucracy (1 value)
  p-g-hign-bureaucracy           ; row 6: additional value for high bureaucracy (1 value)

  ;--------------------------------------------------------------------;
  ; Land use code for UK
  ;--------------------------------------------------------------------;
  p-g-efa-code-list                  ; list of codes for EFA crops and zones

  p-g-max-sq                         ; define max value of soil quality
  ;----------------------------------------------
  ; Threshold values
  ;-----------------------------------------------
  p-g-low-sq-threshold               ; threshold of low soil quality, default 0.5
  p-g-min-area-threshold             ; Generally the minimum area of any AES implementation for farmers.
                                     ; This is to avoid unrealistic AES area when generating AES contracts
  p-g-threshold-lst                  ; list of thresholds (adoption rates)

  ;-----------------------------------------
  ; AES in-field implementation constraints
  ;-----------------------------------------
  p-g-aes-in-field-max-list            ; the maximum proportion of a field can be put under the four AES

  p-g-output-file-folder               ; output file path
  p-g-output-suffix                    ; output file name suffix
  p-g-average-aes-cost                 ; average AES implementation cost across country

  ;----------------------------------------
  ; For integration of regression analysis
  ;----------------------------------------
  p-g-norm-higher-than-mean-lst      ; a list of lists - values higher than mean in a normal distribution
  p-g-norm-lower-than-mean-lst       ; a list of lists - values lower than mean in a normal distribution
  p-g-norm-combined-lst

  ;---------- FOR STATS---------------------
  v-g-stats-openness-fsa               ; sum of openness of FSA farmers at every tick
  v-g-stats-openness                   ; sum of farmers' openness towards the four AES
  v-g-stats-aes-area                   ; sum of all AES contract areas
  v-g-stats-aes-fields                 ; sum of number of AES fields in a run
  v-g-stats-accepted-payment-fsa       ; average accepted payment level for each FSA farmers
  v-g-stats-farm-error-list            ; record the error list - aiming to reduce the computing in the model
  v-g-stats-adoption-rate-list         ; record the farm adoption rate list
]

;-----------------------------------------------------------------------------------;
; Breeds
;-----------------------------------------------------------------------------------;
breed [ farmers farmer ]
breed [ fields field ]
breed [ aes a-aes ]

farmers-own
[
  ;-----------------------------------------------------------------------------------;
  ; Input
  ;-----------------------------------------------------------------------------------;
  p-farm-area                    ; size of farm given by sum of field sizes
  p-farmer-id 		               ; farmer ID read from input file, data format depending on input format
  p-fsa-spec                     ; farm specialization (FSA), read from input file
  p-fsa-size                     ; farm size (FSA) classification, read from input file
  ;p-eco                          ; organic farming practice (1/0)
  p-advisory			               ; if a farmer has access to advisory (1/0)
  p-property-set                 ; agentset of all fields a farmer owns
  p-gis-feature-list             ; GIS feature list of all fields a farmer owns
  p-baseline                     ; adoption in 2019 0/1

  ;-----------------------------------------------------------------------------------;
  ; Attitude and openness
  ;-----------------------------------------------------------------------------------;
  v-prior-experience-list        ; list of farmer's prior experience with each AES (1/0)
  v-social-network               ; agentset of farmers that influence the farmer (neighbors and same FSA)
  v-neighbor?                    ; helper variable for setting up the social-network
  v-open-to-aes-list             ; list of AES the farmer is open to (1/0)
  p-prob-intrinsic-open-list     ; farmer's probability of being intrinsically open to an AES

  p-prob-uptake                  ; probability that a farmer takes up any of the four AES by regression analysis. The predictors of the regression analsysis are:
                                 ; Field_size, Farm_size, Farm_spec, Crop_diversification, Elevation, Precip, Max_temperature and Soil Erosion
  p-prob-uptake-rank             ; the rank from low to high of probability of adoption (i.e., p-prob-update)
  ;-----------------------------------------------------------------------------------;
  ; Willingness to accept (based on DCE)
  ;-----------------------------------------------------------------------------------;
  p-accepted-payment-list        ; list of accepted payment level calculated based on AES contract details and farmer characteristics
  p-accepted-aes-list            ; list indicating for each AES whether offered payment level is larger or equal compared to the accepted payment level (0/1)
  p-envisioned-area-list         ; list of area farmer is willing to use for each AES

  ;-----------------------------------------------------------------------------------;
  ; AES fields
  ;-----------------------------------------------------------------------------------;
  v-suitable-fields-list         ; list of agentset of all fields suitable for each AES
  v-contract-area-list           ; list of total area on which farmer has AES contracts for each AES
  v-nr-aes-fields-list           ; list of number of AES contracts for each AES

  ;---------------
  ; UK CS - Stats
  ;---------------
  v-stats-suitable-fields-list       ; farmer suitable fields
  ;p-low-sq                           ; if a farm has low soil quality (1/0)
  p-stats-farm-area-list             ; read from input file. a list of a farmer's total farm area, arable area, grassland area and horticulture area
  p-envisioned-area-ha-list          ; there is a value check on the area- whether an envisioned area is bigger than total arable land/grassland
  v-gap                              ; list of gaps between envisioned area and realised area
  v-time-decide?                     ; a controller on when a farm makes decsion. Farms don't make decision on participation every year

]

fields-own
[
  p-owner-id                     ; owner ID (farmer)
  p-field-id                     ; field ID
  p-land-use                     ; land use on the field
  p-area                         ; size of the field
  p-EFA                          ; EFA status of the field (1/0)
  p-ess                          ; existing ess of the modelled types in the field (1/0)
  p-soil-quality                 ; soil quality of the field
  p-village                      ; a village where a field is located
  v-aes-list                     ; list of AES currently applied on the field
  v-aes-hist-list                ; list of history of AES previously applied to the field
  ;----------UK CS---------;
  v-aes-area-list                ; JL: list of AES area currently applied on the field
  v-avail-area                   ; JL: available area for aes = p-area - sum v-aes-area-list
]

aes-own
[
  v-aes-name                     ; name of AES (buffer strip, catch crops, grassland, conversion)
  v-aes-nr                       ; number of AES type (0: buffer strip, 1: catch crops, 2: grassland, 3: conversion)
  v-aes-contract-year            ; current duration of AES contract (in years) 				
  v-aes-size		                 ; size of AES contract (field size)
  v-aes-owner                    ; farmer who signed contract for that AES (agentset)
  v-aes-field     	             ; field on which AES is applied (agentset)
  v-aes-id                       ; AES contract ID - generate a meaningful id: pre-x means AES generated at the model initialization stage,
                                 ; x means xth year of the contract; after-y means AES generated during the simulation,
                                 ; y marks the tick that the contract is generated
  v-aes-field-lu                 ; field land use
  v-aes-field-size               ; the field total area

]

;-----------------------------------------------------------------------------------;
; Setup
;-----------------------------------------------------------------------------------;
to setup
  clear-all

  reset-timer

  ; initialise the global variables
  initialise-global-variables

  ; load WTA and area specifications according to AES properties (setup_WTA.nls)
  setup-DCE-specifics

  ; create the landscape (fields) by loading GIS data (setup_fields.nls)
  create-landscape

  ; setup farmers: load input file and define properties (setup_farmers.nls)
  setup-farmers

  reset-ticks

  if not i-nlrx-run?
  [show (word "Setup time: " timer)]
end


;-----------------------------------------------------------------------------------;
; Go
;-----------------------------------------------------------------------------------;
to go
  reset-timer

  ; stop if simulated time span is reached
  if (ticks > i-g-years - 1) [
    update-stats-final
    if i-save-stats?[
      write-model-stats
    ]
    stop ]

  ; Step 0: Update the world
  update-world

  ; Step 1: Openness to specific AES (check_openness.nls)
  check-openness-to-aes

  ; Step 2: Select suitable fields (select_fields.nls)
  select-suitable-fields

  ; Step 3: Deliberation (deliberation.nls)
  ; check for each farmer and AES whether offered-payment is high enough and
  ; choose select fields where AES should be adopted
  deliberate-aes-decision

  ;stats
  update-stats

  if i-save-csv? [
    write-farm-record
    write-aes-record
  ]
  tick
  ;if not i-nlrx-run?
 ; [show (word "Time tick " ticks ": " timer)]
end

;------------------------------------------------------;
; Initialise the global variables in the setup process
;-------------------------------------------------------;
to initialise-global-variables
  ;set up output file directory and file name suffix
  set p-g-output-file-folder (word "../output/"landscape"/")
  if not pathdir:isDirectory? p-g-output-file-folder [pathdir:create p-g-output-file-folder]

  set p-g-output-suffix remove " " date-and-time
  set p-g-output-suffix replace-item 2 p-g-output-suffix "-"
  set p-g-output-suffix replace-item 5 p-g-output-suffix "-"
  set p-g-output-suffix replace-item 8 p-g-output-suffix "-"

  set p-g-low-sq-threshold 0.5
  set p-g-min-area-threshold 0.01             ; according to the adoption data
  set p-g-sd-mean-fraction 0.1
  set p-g-aes-in-field-max-list [0.32 1.0 1.0 1.0]

  ; initialise globle stats variables
  set v-g-stats-aes-area [0 0 0 0]
  set v-g-stats-aes-fields [0 0 0 0]
  set v-g-stats-openness []
  set v-g-stats-accepted-payment-fsa [ ]

  set p-g-record-errors [ ]
  set p-g-adoption-rates []
  set p-g-record-seeds []

  if landscape = "samples_UK" [
    set p-g-threshold-lst [0.09 0 0.04 0]
  ]
  if landscape =  "specialsamples_UK" [
    set p-g-threshold-lst [0.3 0 0.5 0]
  ]
  if landscape = "UK" [
    set p-g-threshold-lst [0.09 0 0.06 0]
  ]



  set p-g-norm-higher-than-mean-lst [ ]      ; a list of lists - values higher than mean in a normal distribution
  set p-g-norm-lower-than-mean-lst  [ ]      ; a list of lists - values lower than mean in a normal distribution
  set p-g-norm-combined-lst [ ]              ; a list of lists - add higher values and lower value together to be one

  ; set the model random seed - only necessary when using locally,
  ; for nlrx set-seed? false
  if i-g-set-seed? [
    set p-g-record-a-seed i-g-model-seed
    random-seed i-g-model-seed
  ]
  if not i-g-set-seed? [
    ;only if it is not run by nlrx, take the control of the seed setting and record it in g-record-seed
    if not i-nlrx-run?
      [
        set p-g-record-a-seed new-seed
        random-seed p-g-record-a-seed
      ]
    if i-go-x-times? [
       set p-g-record-a-seed new-seed
       random-seed p-g-record-a-seed
    ]
  ]
end

to go-x-times
  ;initialise the global vairables
  set p-g-record-errors []
  set p-g-adoption-rates []
  set p-g-record-seeds []

  ;initialise the local variables
  let record-errors []
  let farmer-adoption-rate []
  let record-seeds []
  let aes-pos 0  ; which AES to record
  if i-multi-run-AES = "Mixed AES" [set aes-pos 4]
  if i-multi-run-AES = "Buffer Strips" [set aes-pos 0]
  if i-multi-run-AES = "Cover Crops" [set aes-pos 1]
  if i-multi-run-AES = "Grassland Management" [set aes-pos 2]
  if i-multi-run-AES = "Land Conversion" [set aes-pos 3]

  let i 0
  let m-start date-and-time

  while [i < i-g-times] [
    setup
    go
    if-else i-multi-run-AES != "All AES in list" [
      set record-errors fput (item aes-pos report-farm-adoption-error-list) record-errors
      set farmer-adoption-rate fput (item aes-pos report-farm-adoption-rate-list) farmer-adoption-rate
      set record-seeds fput p-g-record-a-seed record-seeds
      if not i-nlrx-run? [show i]
      set i i + 1
    ]
    [
      set record-errors fput report-farm-adoption-error-list record-errors
      set farmer-adoption-rate fput report-farm-adoption-rate-list farmer-adoption-rate
      set record-seeds fput p-g-record-a-seed record-seeds
      if not i-nlrx-run? [show i]
      set i i + 1
    ]
  ]

  set p-g-record-errors record-errors

  set p-g-adoption-rates farmer-adoption-rate

  if i-multi-run-AES = "All AES in list" [
    ;calculate average output of the runs
    set p-g-record-errors report-mean-list-of-list p-g-record-errors 4
    set p-g-adoption-rates report-mean-list-of-list farmer-adoption-rate 5
  ]

  set p-g-record-seeds record-seeds

  if i-save-stats? [
    write-model-metrics mean record-errors mean farmer-adoption-rate list-to-R-vector record-errors list-to-R-vector farmer-adoption-rate list-to-R-vector p-g-record-seeds
  ]

  let m-end date-and-time
  show (word "Start: " m-start "; End: " m-end ". -----Done!")
  stop
end






	
@#$#@#$#@
GRAPHICS-WINDOW
220
10
978
769
-1
-1
5.0
1
10
1
1
1
0
1
1
1
0
149
0
149
0
0
1
ticks
30.0

BUTTON
10
105
73
138
setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
10
660
160
720
i-g-access-to-advisory
0.8
1
0
Number

INPUTBOX
985
605
1140
665
i-g-payment-buffer-strips
524.0
1
0
Number

INPUTBOX
1145
605
1300
665
i-g-payment-catch-crops
124.0
1
0
Number

INPUTBOX
1305
605
1460
665
i-g-payment-grassland
183.0
1
0
Number

INPUTBOX
1465
605
1620
665
i-g-payment-conversion
321.0
1
0
Number

CHOOSER
985
555
1140
600
i-g-bureaucracy-buffer-strips
i-g-bureaucracy-buffer-strips
"low" "medium" "high"
1

CHOOSER
1465
555
1620
600
i-g-bureaucracy-conversion
i-g-bureaucracy-conversion
"low" "medium" "high"
1

CHOOSER
1145
555
1300
600
i-g-bureaucracy-catch-crops
i-g-bureaucracy-catch-crops
"low" "medium" "high"
1

CHOOSER
1305
555
1460
600
i-g-bureaucracy-grassland
i-g-bureaucracy-grassland
"low" "medium" "high"
1

BUTTON
75
105
138
138
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
125
10
210
70
i-g-model-seed
22500.0
1
0
Number

SWITCH
10
35
120
68
i-g-set-seed?
i-g-set-seed?
1
1
-1000

INPUTBOX
985
670
1140
730
i-g-area-min-buffer-strips
0.0
1
0
Number

INPUTBOX
1145
670
1300
730
i-g-area-min-catch-crops
0.0
1
0
Number

INPUTBOX
1305
670
1460
730
i-g-area-min-grassland
0.0
1
0
Number

INPUTBOX
1465
670
1620
730
i-g-area-min-conversion
0.0
1
0
Number

INPUTBOX
125
450
210
510
i-g-years
1.0
1
0
Number

CHOOSER
10
785
165
830
i-g-social-network-type
i-g-social-network-type
"none" "neighbours" "FSA"
0

INPUTBOX
10
830
165
890
i-g-social-network-radius
5.0
1
0
Number

CHOOSER
10
335
157
380
landscape
landscape
"specialsamples_UK" "samples_UK" "UK"
1

PLOT
1270
10
1555
230
AES Uptake
tick
AES Contracts
0.0
20.0
0.0
120.0
true
true
"" ""
PENS
"Total" 1.0 0 -16777216 true "" "plot count aes"
"BF" 1.0 0 -12087248 true "" "plot count aes with [v-aes-name = \"buffer-strips\"]"
"CC" 1.0 0 -5207188 true "" "plot count aes with [v-aes-name = \"catch-crops\"]"
"MG" 1.0 0 -11033397 true "" "plot count aes with [v-aes-name = \"grassland\"]"
"CNV" 1.0 0 -1604481 true "" "plot count aes with [v-aes-name = \"conversion\"]"

PLOT
980
10
1260
230
FSA 
FSA P1 - P5
No. of Farmers
1.0
20.0
0.0
100.0
true
false
"" ""
PENS
"FSA" 0.5 1 -16777216 true "" "histogram [p-fsa-spec] of farmers\nset-plot-x-range 1 6"

PLOT
1270
235
1555
455
AES per FSA
ticks
AES Contracts
0.0
20.0
0.0
80.0
true
true
"" ""
PENS
"P1" 1.0 0 -12087248 true "" "plot-fsa-aes-uptake 1"
"P2" 1.0 0 -5207188 true "" "plot-fsa-aes-uptake 2"
"P3" 1.0 0 -12895429 true "" "plot-fsa-aes-uptake 3"
"P4" 1.0 0 -2674135 true "" "plot-fsa-aes-uptake 4"
"P5" 1.0 0 -13345367 true "" "plot-fsa-aes-uptake 5"

TEXTBOX
990
475
1140
493
BS: buffer strips (flower)
11
0.0
1

TEXTBOX
1155
475
1305
493
CC: catch-crops (plant)
11
0.0
1

TEXTBOX
1315
475
1465
501
MG: maintaining grassland (dot)
11
0.0
1

TEXTBOX
1475
475
1620
516
CVN: conversion of arable land to grassland (x)
11
0.0
1

TEXTBOX
245
610
275
650
■
40
45.0
1

TEXTBOX
245
639
275
679
■
40
55.0
1

TEXTBOX
245
669
275
715
■
40
115.0
1

TEXTBOX
285
634
340
652
arable land
11
0.0
1

TEXTBOX
285
664
335
682
grassland
11
0.0
1

TEXTBOX
285
694
345
712
horticulture
11
0.0
1

TEXTBOX
730
790
880
808
For testing purposes:
14
0.0
1

TEXTBOX
245
700
270
745
■
40
5.0
1

TEXTBOX
285
725
435
743
other
11
0.0
1

PLOT
1565
10
1875
230
AES: Fraction of farmers
NIL
NIL
0.0
20.0
0.0
0.5
true
true
"" ""
PENS
"Total" 1.0 0 -16777216 true "" "plot count farmers with [ sum v-contract-area-list > 0 ] / count farmers"
"BF" 1.0 0 -12087248 true "" "plot count farmers with [ item 0 v-contract-area-list > 0 ] / count farmers"
"CC" 1.0 0 -5207188 true "" "plot count farmers with [ item 1 v-contract-area-list > 0 ] / count farmers"
"MG" 1.0 0 -11033397 true "" "plot count farmers with [ item 2 v-contract-area-list > 0 ] / count farmers"
"CNV" 1.0 0 -1604481 true "" "plot count farmers with [ item 3 v-contract-area-list > 0 ] / count farmers"

PLOT
1565
235
1875
455
AES: Fraction of fields
NIL
NIL
0.0
20.0
0.0
0.2
true
true
"" ""
PENS
"Total" 1.0 0 -16777216 true "" "plot sum [p-area] of fields with [ sum v-aes-list > 0 ] / sum [p-area] of fields\n"
"BF" 1.0 0 -12087248 true "" "plot sum [p-area] of fields with [ item 0 v-aes-list > 0 ] / sum [p-area] of fields"
"CC" 1.0 0 -5207188 true "" "plot sum [p-area] of fields with [ item 1 v-aes-list > 0 ] / sum [p-area] of fields"
"MG" 1.0 0 -11033397 true "" "plot sum [p-area] of fields with [ item 2 v-aes-list > 0 ] / sum [p-area] of fields"
"CNV" 1.0 0 -1604481 true "" "plot sum [p-area] of fields with [ item 3 v-aes-list > 0 ] / sum [p-area] of fields"

INPUTBOX
10
540
162
600
i-g-prob-open-experience
0.0
1
0
Number

INPUTBOX
10
600
160
660
i-g-prob-open-advisory
0.5
1
0
Number

INPUTBOX
10
720
160
780
i-g-prob-open-social
0.1
1
0
Number

CHOOSER
495
810
655
855
i-g-site-selection
i-g-site-selection
"highest-payment" "highest-payment-diff" "highest-payment-ratio" "largest-area"
2

CHOOSER
985
505
1140
550
i-g-duration-buffer-strips
i-g-duration-buffer-strips
1 5 10
1

CHOOSER
1145
505
1300
550
i-g-duration-catch-crops
i-g-duration-catch-crops
1 5 10
1

CHOOSER
1305
505
1460
550
i-g-duration-grassland
i-g-duration-grassland
1 5 10
1

CHOOSER
1465
505
1620
550
i-g-duration-conversion
i-g-duration-conversion
1 5 10
1

SWITCH
1030
785
1215
818
i-g-start-with-existing-aes?
i-g-start-with-existing-aes?
1
1
-1000

SWITCH
120
405
220
438
i-save-csv?
i-save-csv?
1
1
-1000

INPUTBOX
1290
820
1360
880
nlrx_id
NIL
1
0
String

SWITCH
1365
805
1465
838
i-nlrx-run?
i-nlrx-run?
1
1
-1000

SWITCH
10
405
120
438
i-save-stats?
i-save-stats?
1
1
-1000

INPUTBOX
730
825
815
885
base-WTA-bs
530.0
1
0
Number

INPUTBOX
815
825
895
885
base-WTA-cc
210.0
1
0
Number

INPUTBOX
895
825
975
885
base-WTA-gm
183.0
1
0
Number

INPUTBOX
975
825
1065
885
base-WTA-cvn
460.0
1
0
Number

INPUTBOX
1065
825
1145
885
i-sd-mean-fraction
0.1
1
0
Number

BUTTON
10
180
90
213
NIL
go-x-times
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
140
105
203
138
NIL
stop
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
170
785
275
845
i-lambda-openness
24.0
1
0
Number

INPUTBOX
100
155
180
215
i-g-times
50.0
1
0
Number

SWITCH
1365
845
1492
878
i-go-x-times?
i-go-x-times?
1
1
-1000

CHOOSER
10
220
187
265
i-multi-run-AES
i-multi-run-AES
"All AES in list" "Buffer Strips" "Cover Crops" "Grassland Management" "Land Conversion"
0

CHOOSER
295
810
480
855
i-how-integrate-regression
i-how-integrate-regression
"no" "regression-yes-no-adoption" "regression-sorted-score"
2

SWITCH
870
785
1027
818
i-test-WTA-mode?
i-test-WTA-mode?
1
1
-1000

TEXTBOX
15
15
165
33
Set seeds:
14
0.0
1

TEXTBOX
15
155
165
173
Multiple runs:
14
0.0
1

TEXTBOX
15
80
165
98
A single run:
14
0.0
1

TEXTBOX
15
315
165
333
Farms and fields:
14
0.0
1

TEXTBOX
15
385
165
403
Output:
14
0.0
1

TEXTBOX
10
455
160
473
Duration of a run:
14
0.0
1

TEXTBOX
990
445
1140
463
Offered AES:
14
0.0
1

TEXTBOX
300
785
450
803
Model choices:
14
0.0
1

TEXTBOX
500
785
650
803
Famers' reasoning:
14
0.0
1

TEXTBOX
10
515
160
533
Openness:
14
0.0
1

TEXTBOX
1255
790
1405
808
For using R nlrx:
14
0.0
1

MONITOR
735
25
947
70
Adoption rate: BS,CC,MG,CVN,Total
report-farm-adoption-rate-list
17
1
11

BUTTON
10
270
180
303
Display adoption rates
show (word \"adoption rates \" p-g-record-errors) 
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

MONITOR
990
260
1245
305
Proportion of farmers being open to each AES (%)
report-stats-openness
17
1
11

MONITOR
765
75
920
120
AES adoption area (ha)
report-aes-adoption-area
17
1
11

SWITCH
985
735
1140
768
i-g-advisory-BS?
i-g-advisory-BS?
1
1
-1000

SWITCH
1145
735
1295
768
i-g-advisory-CC?
i-g-advisory-CC?
1
1
-1000

SWITCH
1305
735
1460
768
i-g-advisory-MG?
i-g-advisory-MG?
1
1
-1000

SWITCH
1470
735
1625
768
i-g-advisory-CVN?
i-g-advisory-CVN?
1
1
-1000

@#$#@#$#@
## WHAT IS IT?

The model is simulating Humber farmers' decision-making process on AES adoptions. It focuses on four types of AES: buffer strips (BS), cover crops (CC), grassland management (GM) and conversion of arable land to grassland (CVN). The model can be used to study different AES designs' impact on farm adoptions at macro level.

## HOW IT WORKS

The agents in the model include farmers, fields and AES contracts. Each farmer agent has a set of field agents. A farmer agent decides to take up an AES based on (1) whether the farmer agent is open to the AES; (2) whether the farmer agent has suitable fields to implement the AES; and (3) whether the offered payment is higher than the amount of money that the farmer is willing to accept (WTA). When three conditions are satisfied, the farmer agent will choose a field to sign up the AES, which generates an AES contract agent in the model.

At the macro level, we look at farmers' adoption rate of the four types of AES in the whole region.


## HOW TO USE IT

The model interface has different sections containing various variables to set up before running the model and display the model output.

<b/>(1) "Set seeds" section:</b>  
<i/>i-g-set-seed?</i> is used to decide whether the seed of a run is manually set up or not. if it’s true, the seed is set manually via interface. Otherwise, the seed is set automatically during the model run.  
<i/>i-g-model-seed</i> is an user-input for manually setting the seed. The variable will be in use when i-g-set-seed? is true.
<b/>(2) “A single run” section:</b>
The functions include:  “setup” function initialises the model, “go” is to run the model and “stop” is to stop a run of the model.
<b/>(3) “Multiple runs” section:</b>
This section is implemented for running multiple times and investigating the average adoption rates of the multiple runs. This module is designed to get the average outputs directly rather than setting up experiments in Behaviour Space. The number of runs is set by <i/>i-g-times</i>; <i/>i-multi-run-AES</i> defines which AES adoption to be inspected: if “All AES in list” is chosen, the result will include the average adoption rates of BS, CC, GM, CVN and the total farm adoption rates; if individual AES is chosen, the result will only include the average adoption rate of the selected AES. The results will be displayed in Command Centre by click the “Display adoption rates” button after the simulation finishes. 
<b/>(4) “Farms and fields” section:</b>
In this section, a specific landscape is chosen. The available choices are two different UK farms and fields’ samples and the farms and fields of the whole Humber region.
<b/>(5) “Output” section: </b>
This section controls whether the model saves csv files after a simulation is finished.
<i/>i-save-csv?</i> saves output files of the variable values of all agents at every tick, if it’s true;
<i/>i-save-stats?</i> saves the statistics of the model output at the end of a simulation run, if it’s true. The statistics include farmers adoption rates, model prediction errors, the proportion of different FSA farmers being open at the end of a simulation, the average WTA in the farmer population for the four modelled AES, the average WTA of different FSA farmers for the four AES, the average WTA of different sizes of farmers for the four AES.
<b/>(6) “Duration of a run” section:</b>
<i/>i-g-years</i> defines the duration of a simulation run. By default, it is set to be 1.
<b/>(7) “Openness” section:</b>
<i/>i-g-prob-open-experience</i> is the probability that a farmer with prior experience is open;
<i/>i-g-prob-open-advisory</i> is the probability that a farmer with access to advisory is open;
<i/>i-g-access-to-advisory</i> is the probability that a farmer has access to advisory;
<i/>i-g-prob-open-social</i> is the probability that a farmer with positive social influence is open;
i-g-social-network-type defines which other farmers have an influence, including no other farmers, spatial neighbours, other farmers belong to the same FSA and other farmers in the same village);
<i/>i-g-social-network-radius</i> defines the radius in which other farmers are considered if influence is from spatial neighbours.
<i/>i-lambda-openness</i> is the intrinsic openness factor, which is the time of farmer adoption rate, as we assume that the intrinsic openness towards an AES is proportional to the farmer adoption rates.
<b/>(8) “Model choices” section:</b>
<i/>i-how-integrate-regression</i> defines the model how to integrate regression analysis results. "no" means no integration of the farm-level regression analysis results; "regression-yes-no-adoption" means to integrate yes-no regression model output, which is used to decide WTA to be above the mean WTA or below the mean WTA; "regression-sorted-score" means to decide WTAs of the farmers based on the sorted regression model output scores.
<b/>(8) “Farmers’ reasoning” section:</b>
<i/>i-g-site-selection</i> is farmers’ preference when they determine which accepted scheme is the most profitable
<b/>(9) “Offered AES” section:</b>
This section includes the input parameters of the modelled AES. Four aspects of AES characteristics are included: the duration of a contract (<i/>i-g-duration-AES</i>), the bureaucracy level(<i/>i-g-bureaucracy-AES</i>), the offered payment (<i/>i-g-payment-AES</i>) and the minimal plot size to implement the AES on (<i/>i-g-area-min-AES</i>)
<b/>(10) “For using R nlrx” section</b>
<i/>nlrx_id</i> is a variable needed when running the NetLogo model using R package nlrx and writing the output files in NetLogo instead of nlrx default output files. It transfers the current nlrx experiment name, random seed and runnumber (siminputrow) to NetLogo. For more information, please refer to “Self-written NetLogo output” section in https://cran.r-project.org/web/packages/nlrx/vignettes/furthernotes.html
<i/>i-go-x-times?</i> is to generate a random seed for each run when go-x-times function is called.
<b/>(11) “For testing purposes” section:</b>
This section of variables are designed for running the test of the model in order to explore farmer adoptions under different WTA distributions. <i/>i-test-WTA-mode?</i> is to activate the testing functions when it is true. The WTA distribution is determined by <i/>base-WTA-bf</i>, <i/>base-WTA-cc</i>, <i/>base-WTA-gm</i>, <i/>base-WTA-cvn</i> and <i/>i-sd-mean-fraction</i>. <i/>i-g-start-with-existing-aes?</i> sets the farmers' initial status - If true, the farmers start with existing AES in 2016 at the beginning of a simulation. If false, the simulation starts with no adoption, representing the time before countryside stewardship schemes were rolled out. By default, the variable is set to be false.


## THINGS TO TRY

To test farmers’ adoptions in different scenarios, you could try out the variables in the “Offered AES” section. 

## EXTENDING THE MODEL



## NETLOGO FEATURES



## RELATED MODELS


The BESTMAP-ABM-UK is a part of the BESTMAP model suite. Five ABMs are developed in the model suite for five case studies in Czech Republic, Germany, Serbia, Spain and the UK. 

## CREDITS AND REFERENCES

The model is developed under the BESTMAP ABM team’s effort, including Chunhui Li, Meike Will, Nastasija Grujić, Jiaqi Ge and Birgit Müller.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.2.2
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="base_WTA_bf4B" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>count aes with [v-aes-name = "buffer-strips"]</metric>
    <metric>count aes with [v-aes-name = "catch-crops"]</metric>
    <metric>count aes with [v-aes-name = "grassland"]</metric>
    <metric>count aes with [v-aes-name = "conversion"]</metric>
    <metric>count farmers with [ item 0 v-contract-area-list &gt; 0 ]</metric>
    <metric>count farmers with [ item 1 v-contract-area-list &gt; 0 ]</metric>
    <metric>count farmers with [ item 2 v-contract-area-list &gt; 0 ]</metric>
    <metric>count farmers with [ item 3 v-contract-area-list &gt; 0 ]</metric>
    <metric>report-stats-average-accepted-payment</metric>
    <steppedValueSet variable="base-WTA-bf" first="500" step="200" last="1500"/>
  </experiment>
  <experiment name="preference_site_selection2B" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>count aes</metric>
    <metric>count aes with [v-aes-name = "buffer-strips"]</metric>
    <metric>count aes with [v-aes-name = "catch-crops"]</metric>
    <metric>count aes with [v-aes-name = "grassland"]</metric>
    <metric>count aes with [v-aes-name = "conversion"]</metric>
    <metric>count farmers with [sum v-contract-area-list &gt; 0] / count farmers</metric>
    <metric>count farmers with [item 0 v-contract-area-list &gt; 0] / count farmers</metric>
    <metric>count farmers with [item 1 v-contract-area-list &gt; 0] / count farmers</metric>
    <metric>count farmers with [item 2 v-contract-area-list &gt; 0] / count farmers</metric>
    <metric>count farmers with [item 3 v-contract-area-list &gt; 0] / count farmers</metric>
    <metric>report-stats-average-accepted-payment</metric>
    <metric>report-stats-sd-accepted-payment</metric>
    <metric>g-record-seed</metric>
    <enumeratedValueSet variable="i-g-site-selection">
      <value value="&quot;highest-payment&quot;"/>
      <value value="&quot;highest-payment-diff&quot;"/>
      <value value="&quot;highest-payment-ratio&quot;"/>
      <value value="&quot;largest-area&quot;"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="_distributed-WTA-max-adoption2" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>count aes with [v-aes-name = "buffer-strips"]</metric>
    <metric>count aes with [v-aes-name = "catch-crops"]</metric>
    <metric>count aes with [v-aes-name = "grassland"]</metric>
    <metric>count aes with [v-aes-name = "conversion"]</metric>
    <metric>count farmers with [ item 0 v-contract-area-list &gt; 0 ] / count farmers</metric>
    <metric>count farmers with [ item 1 v-contract-area-list &gt; 0 ] / count farmers</metric>
    <metric>count farmers with [ item 2 v-contract-area-list &gt; 0 ] / count farmers</metric>
    <metric>count farmers with [ item 3 v-contract-area-list &gt; 0 ] / count farmers</metric>
    <metric>v-g-stats-accepted-payment</metric>
  </experiment>
  <experiment name="_distributed-WTA-max-adoption2" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>count aes with [v-aes-name = "buffer-strips"]</metric>
    <metric>count aes with [v-aes-name = "catch-crops"]</metric>
    <metric>count aes with [v-aes-name = "grassland"]</metric>
    <metric>count aes with [v-aes-name = "conversion"]</metric>
    <metric>count farmers with [ item 0 v-contract-area-list &gt; 0 ] / count farmers</metric>
    <metric>count farmers with [ item 1 v-contract-area-list &gt; 0 ] / count farmers</metric>
    <metric>count farmers with [ item 2 v-contract-area-list &gt; 0 ] / count farmers</metric>
    <metric>count farmers with [ item 3 v-contract-area-list &gt; 0 ] / count farmers</metric>
    <metric>v-g-stats-accepted-payment</metric>
  </experiment>
  <experiment name="social-network" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>count aes</metric>
    <metric>count aes with [v-aes-name = "buffer-strips"]</metric>
    <metric>count aes with [v-aes-name = "catch-crops"]</metric>
    <metric>count aes with [v-aes-name = "grassland"]</metric>
    <metric>count aes with [v-aes-name = "conversion"]</metric>
    <metric>count farmers with [sum v-contract-area-list &gt; 0] / count farmers</metric>
    <metric>count farmers with [item 0 v-contract-area-list &gt; 0] / count farmers</metric>
    <metric>count farmers with [item 1 v-contract-area-list &gt; 0] / count farmers</metric>
    <metric>count farmers with [item 2 v-contract-area-list &gt; 0] / count farmers</metric>
    <metric>count farmers with [item 3 v-contract-area-list &gt; 0] / count farmers</metric>
    <metric>report-stats-average-accepted-payment</metric>
    <metric>report-stats-sd-accepted-payment</metric>
    <steppedValueSet variable="i-g-prob-open-social" first="0" step="0.2" last="1"/>
  </experiment>
  <experiment name="exp_50runs" repetitions="1" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>item 2 report-farm-adoption-rate-list</metric>
    <metric>item 2 report-farm-adoption-error-list</metric>
    <metric>report-farm-adoption-rate-list</metric>
    <metric>report-farm-adoption-error-list</metric>
    <metric>p-g-stats-openness</metric>
    <metric>list-to-R-vector report-farm-adoption-rate-list</metric>
    <metric>list-to-R-vector report-farm-adoption-error-list</metric>
    <metric>list-to-R-list p-g-stats-openness</metric>
    <steppedValueSet variable="i-g-model-seed" first="0" step="500" last="25000"/>
  </experiment>
  <experiment name="lambda_openness_1" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>count farmers with [ item 0 v-contract-area-list &gt; 0 ]</metric>
    <metric>count farmers with [ item 1 v-contract-area-list &gt; 0 ]</metric>
    <metric>count farmers with [ item 2 v-contract-area-list &gt; 0 ]</metric>
    <metric>count farmers with [ item 3 v-contract-area-list &gt; 0 ]</metric>
    <metric>report-farm-adoption-rate-list</metric>
    <metric>report-farm-adoption-error-list</metric>
    <metric>list-to-R-vector report-farm-adoption-rate-list</metric>
    <metric>list-to-R-vector report-farm-adoption-error-list</metric>
    <metric>list-to-R-list p-g-stats-openness</metric>
    <steppedValueSet variable="lambda-openness" first="1" step="2" last="13"/>
  </experiment>
  <experiment name="regression" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <metric>item 2 report-farm-adoption-error-list</metric>
    <metric>item 2 report-farm-adoption-rate-list</metric>
    <metric>report-farm-adoption-rate-list</metric>
    <metric>report-farm-adoption-error-list</metric>
    <metric>list-to-R-vector report-farm-adoption-rate-list</metric>
    <metric>list-to-R-vector report-farm-adoption-error-list</metric>
    <metric>list-to-R-list p-g-stats-openness</metric>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
1
@#$#@#$#@
