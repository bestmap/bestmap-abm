;-----------------------------------------------------------------------------------;
; Step 3: Deliberation
; check for each farmer and AES whether offered-payment is high enough and 
; select fields where AES is adopted
;-----------------------------------------------------------------------------------;
to deliberate-aes-decision
  ask farmers
  [  
    ;check whether all the envisioned area realised
    ;farmers who have fulfilled the AES vision don't need to go through these steps 
    set v-gap (map [[a b] -> precision (a - b) 4] p-envisioned-area-ha-list  v-contract-area-list)

    ;----------------
    ; Debug
    ;----------------
    let farm-id p-farmer-id
    
    ; check if the gap between the envisioned area and the realised area is bigger than minimum area that is required.
    ; if the gap is bigger, then set the gap-indicatior to be true, which indicates more fields are considered to be put under the AES
    let gap-indicator [1 1 1 1]
    foreach v-gap [i ->
      let p position i v-gap
      let x ifelse-value (i > p-g-min-area-threshold) [1] [0]
      set gap-indicator replace-item p gap-indicator x
    ]
    
    if v-time-decide? [
      ; farmers compare offered payment and accepted payment
      set p-accepted-aes-list r-compare-payment
      
      ; exclude AES to which farmer is not open
      ; modified by JL - if any type of AES has been fullfilled, skip it for deliberation
      ; maybe add accepted-open for stats
      let accepted-open (map [ [ a b c] -> a * b * c ] p-accepted-aes-list v-open-to-aes-list gap-indicator)
    
      ; select fields that are suitable for AES the farmer accepts and is open to
      ; (function in select_fields.nls)
      set v-suitable-fields-list r-suitable-fields-accepted v-suitable-fields-list accepted-open
      
      ; farmers compare contract area and envisioned area to decide whether additional
      ; contracts are needed for specific AES (function in select_fields.nls)
      set v-suitable-fields-list r-compare-realized-contracts v-suitable-fields-list
      
      ; farmers choose which contract to apply on which fields
      ;choose-suitable-contract
      ; modified by JL
      choose-suitable-contract-uk accepted-open
    ]
  ]
end

;-----------------------------------------------------------------------------------;
; farmers compare offered payment and accepted payment
;-----------------------------------------------------------------------------------;
to-report r-compare-payment
  report (map [ [ a b ] -> ifelse-value (a >= b) [ 1 ] [ 0 ] ] 
    p-g-offered-payment-list p-accepted-payment-list)
end

;-----------------------------------------------------------------------------------;
; farmers choose which contract to apply on which fields
; modified by JL: added the variable accepted-open-list to choose-suitable-contract
;-----------------------------------------------------------------------------------;
to choose-suitable-contract-uk [accepted-open-list]
  ; decide order of AES decision based on the highest difference between 
  ; offered payment level and accepted-payment for all AES that farmer would accept
  
  ; decide order of AES application based on selected decision factor
  let aes-decision-order get-aes-decision-order i-g-site-selection accepted-open-list
  

  ; check for each AES farmer accepts and is open to where to adopt AES
  while [ max aes-decision-order > 0 ]
  [
    ; get index of AES with maximum gain per ha in list, remove that AES to prevent further decisions in same year
    let which-aes position (max aes-decision-order) aes-decision-order
    
    set aes-decision-order replace-item which-aes aes-decision-order 0
    
    ; select fields on which no AES is already applied, update after previous decisions
    let possible-fields item which-aes v-suitable-fields-list 
    
    set v-suitable-fields-list replace-item which-aes v-suitable-fields-list 
    fields-available-for-aes possible-fields which-aes
   
   
    ; select fields on which to put the selected AES (given through index 
    ; 'which-aes'), consider only AES for which enough area is available  
    if (any? item which-aes v-suitable-fields-list) [ select-aes-fields-uk which-aes]
    ;if which-aes = 0 [show "select-aes-fields-uk -- buffer strips"]
  
    
  ]
end 

;-----------------------------------------------------------------------------------;
; get order in which fields for AES decision are selected
;-----------------------------------------------------------------------------------;
to-report get-aes-decision-order [ decision-factor t-accepted-aes-list]
  let temp-decision-order [ 0 0 0 0 ]
  
  ; highest offered payment
  if decision-factor = "highest-payment"
  [ set temp-decision-order (map [ [ a b ] -> a * b ] 
    t-accepted-aes-list p-g-offered-payment-list) ]
  
  ; highest difference between offered payment level and accepted-payment
  if decision-factor = "highest-payment-diff"
  [ set temp-decision-order (map [ [ a b c ] -> a * (b - c) ] 
    t-accepted-aes-list p-g-offered-payment-list p-accepted-payment-list) 
  ;  show (word "offered: " p-g-offered-payment-list " ; accepted: " p-accepted-payment-list)
  ]
  
  ; highest ratio between offered payment level and accepted-payment
  ;18/05/2022 JL modified
  if decision-factor = "highest-payment-ratio"
  [ set temp-decision-order (map [ [ a b c ] -> a * (b / (c + 0.01)) ] 
    t-accepted-aes-list p-g-offered-payment-list p-accepted-payment-list) ]
  
  ; largest envisioned area
  if decision-factor = "largest-area"
  [ set temp-decision-order (map [ [ a b ] -> a * b ] 
    t-accepted-aes-list p-envisioned-area-ha-list) ]
    
  report temp-decision-order
end
	
;--------------------------------------------------------------------
; UK special: Check fields whether a scheme is already in place and
; allow compatible schemes on the same fields
;--------------------------------------------------------------------
to-report fields-available-for-aes [field-list which-aes]
  let result []
 ; show (word "input: " field-list " which-aes: " which-aes )
 ; let r ""
  if any? field-list [
  ;foreach field-list [
    ask field-list [
;      set r (word r p-field-id " - " v-aes-list " ; ")
      let indicator item which-aes v-aes-list      ;v-aes-list is the list of AES currently applied on the field
      
      ;if buffer strip (which-aes = 0) is not in the field, it can be put here as it compatible with other schemes
      if (which-aes = 0) and (indicator = 0) [
        set result lput self result
      ]
      ; Other three types of AES are not compatible, therefore if any of them 
      ; is implemented in the field, the other 2 types cannot be signed for
      if (which-aes > 0) and (indicator = 0) [
        let sub-aes-list (sublist v-aes-list 1 4)
        ; if none of the three schemes are in this field, this field can be signed for any of the three schemes
        if (sum sub-aes-list = 0)[
          set result lput self result]
      ]
      ; In case that which-aes >0 and indicator > 0, return empty turtle-set
    ]
   
  ]
  report turtle-set result
end

;-----------------------------------------------------------------------------------;
; select fields on which to put the selected AES
;-----------------------------------------------------------------------------------;
to select-aes-fields-uk [ which-aes ]
  
  let application-list []  
  ;modified by JL
  let required-temp item which-aes v-gap
 
  if required-temp < 0 [show (word "WARNing!!!! required-temp: " required-temp)]
  
  if required-temp > p-g-min-area-threshold                        ; if the gap is bigger than minimal of any AES implementation
  [                  
    let field-list sort item which-aes v-suitable-fields-list      ; 'sort' to return a list  
    let count-aes-fields 0                                         ; a temporary variable to count aes fields
    let contract-area-temp item which-aes v-contract-area-list     ; temporary area variable to record how much area put for AES
    
    let total-suitable-area sum [ p-area ] of item which-aes v-suitable-fields-list
    set total-suitable-area precision total-suitable-area 2

    ; if available field size is smaller or equal to the envisioned area on which 
    ; farmer intends to apply AES: select all fields for AES
    ;ifelse length field-list = 1 OR 
    ;sum [ v-avail-area ] of item which-aes v-suitable-fields-list <= required-temp
    ifelse length field-list = 1 OR total-suitable-area <= required-temp
    [ 
      set application-list field-list 
      ;; update number of aes fields
      ; set v-nr-aes-fields replace-item which-aes v-nr-aes-fields length field-list     
    ]
    [       
      ; choose patches with lowest soil quality; if two fields with same soil quality, smaller field is chosen first
      let sorted-fields sort-by [ [a b] -> ([ p-soil-quality ] of a < [ p-soil-quality ] of b) or 
        ([ p-soil-quality ] of a = [ p-soil-quality ] of b and [ p-area ] of a < [ p-area ] of b) ] 
      (field-list)
      
      ; get cumulative field sizes with increasing soil quality
      let sorted-fields-size map [ p -> [ p-area ] of p ] sorted-fields
      let cum-field-size partial-sums sorted-fields-size
      set cum-field-size (map [ i -> precision i 2 ] cum-field-size)
      
      ; select fields until min. contract size is reached, i.e. farmers adopt more than needed 
      ; if the field size is not exactly reached by fields with lower soil quality
      let nr-selected-fields sum (map [ i -> (ifelse-value i < required-temp [ 1 ] [ 0 ]) ] cum-field-size) + 1
      set v-nr-aes-fields-list replace-item which-aes v-nr-aes-fields-list nr-selected-fields
      
      ;set application-list sublist sorted-fields 0 nr-selected-fields
      ;modified by JL on 4/4/2022
      ifelse nr-selected-fields <= length sorted-fields
      [set application-list sublist sorted-fields 0 nr-selected-fields]
      [set application-list sublist sorted-fields 0 length sorted-fields
       show ("----------------warning! deliberation select-aes-fields-uk sublist")]
      
      set application-list sort-on [p-area] turtle-set application-list    
    ]
    
    ask turtle-set application-list
    [
     ; maximum of AES area in the field
      ;let t-portion random-float item which-aes p-g-aes-in-field-max-list ; the value could be 0
      let area precision (p-area * item which-aes p-g-aes-in-field-max-list) 4
      let aes-area 0                                   ; temporary variable
      if (required-temp > p-g-min-area-threshold) and 
      (area > p-g-min-area-threshold) and (v-avail-area > p-g-min-area-threshold)
      ; make sure the AES contract area not too small and there is available area for AES
      [    
        set v-aes-list replace-item which-aes v-aes-list 1
       ; set v-aes-hist-list fput (item which-aes p-g-aes-type-list) v-aes-hist-list
        set v-aes-hist-list replace-item which-aes v-aes-hist-list 1
        
        ;update the record of aes area in field agent
        set aes-area min (list area required-temp v-avail-area)
        set v-aes-area-list replace-item which-aes v-aes-area-list aes-area
        set v-avail-area precision (p-area - sum v-aes-area-list) 4
        if v-avail-area < 0 [show "----------------warning!!!!v-avail-area < 0 ---------------"]
        ; create AES
        hatch-aes 1 
        [ 
          set v-aes-id (word "after-" ticks)
          set v-aes-name item which-aes p-g-aes-type-list
          ; define AES properties         
          setup-aes aes-area
          set count-aes-fields count-aes-fields + 1
        ] 
        
        ;update envisioned-area realisation. if there are still some area not realised, put it in another field        
        set required-temp precision (required-temp - aes-area) 4
        
        ;set contract-area-temp contract-area-temp + p-area ;; need to be modified
        ;update farmer agent
        set contract-area-temp precision (contract-area-temp + aes-area) 4    ;modified by JL
      ]
    ]
   ; update number of aes fields    
   let c item which-aes v-nr-aes-fields-list
   set v-nr-aes-fields-list replace-item which-aes v-nr-aes-fields-list (count-aes-fields + c)
   
   ;update v-contract-area-list 
   set v-contract-area-list replace-item which-aes v-contract-area-list contract-area-temp
     
  ]
end

;-----------------------------------------------------------------------------------;
; create AES on selected field and set field and farmer related variables
;-----------------------------------------------------------------------------------;
to setup-aes [contract-area]
  set shape (ifelse-value 
    v-aes-name = "buffer-strips" [ "flower" ]
    v-aes-name = "catch-crops" [ "plant" ]
    v-aes-name = "grassland" [ "dot" ]
    [ "x" ]) ; conversion
  
  set v-aes-nr (ifelse-value 
    v-aes-name = "buffer-strips" [ 0 ]
    v-aes-name = "catch-crops" [ 1 ]
    v-aes-name = "grassland" [ 2 ]
    [ 3 ]) ; conversion
  
  set color black
  set size 2
  
  ;; JL: set to year 0 when the AES is generated. 
  ;; Farmers usually make an application and the contract starts the next year if the application is approved 
  set v-aes-contract-year 0
  
  ;;JL: because AES adoption is not necessarily the whole parcel, the code needs to be 
  ; modified using envisioned area, instead of the field p-area  
  set v-aes-size contract-area
  ;set v-aes-size [ p-area ] of myself 
  set v-aes-owner [ p-owner-id ] of myself
  set v-aes-field [ p-field-id ] of myself
  set v-aes-field-lu [p-land-use] of myself
  set v-aes-field-size [p-area] of myself
end
