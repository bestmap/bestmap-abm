# SOFTWARE LICENCE

BESTMAP-ABM-UK is an agent-based model to determine the adoption and spatial allocation of agri-environmental schemes by individual farmers in the Humber region in England.

## Copyright Notice

Copyright © 2023, University of Leeds. All rights reserved.

***Contact:***

Chunhui Li (E-mail: <C.Li2@leeds.ac.uk>)

This program is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with this program.
It can be found in the files `COPYING` provided with this software.
The complete GNU license text can also be found at < https://www.gnu.org/licenses/>.
