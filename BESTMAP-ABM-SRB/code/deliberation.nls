;-----------------------------------------------------------------------------------;
; Step 3: Deliberation
; check for each farmer and AES whether offered-payment is high enough and 
; select fields where AES is adopted
;-----------------------------------------------------------------------------------;
to deliberate-aes-decision
  ; calculate WTA and assign to farmers according to AES specifications and their preferences
  calculate-farmer-wta
  
  ask farmers
  [   
    ; farmers compare offered payment and accepted payment - check which aes regarding a payment
    set p-accepted-aes-list (map [ [ a b ] -> ifelse-value (a >= b) [ 1 ] [ 0 ] ] p-g-offered-payment-list p-accepted-payment-list)
    
    ; exclude AES to which farmer is not open
    let t-accepted-open-list (map [ [ a b ] -> a * b ] p-accepted-aes-list v-open-to-aes-list) 
    
    ; select fields that are suitable for AES the farmer accepts and is open to
    ; (function in select_fields.nls)
    ; v-suitable-fields-list - an agentsset, where each aes can be applied based on land use and size
    set v-suitable-fields-list r-suitable-fields-accepted v-suitable-fields-list t-accepted-open-list
    
    ; farmers compare contract and envisioned area (function in select_fields.nls)
    set v-suitable-fields-list r-compare-realized-contracts v-suitable-fields-list
    
    ; farmers choose which contract to apply on which fields
    with-local-randomness [ choose-suitable-contract ] 

    
  ] 
end


;-----------------------------------------------------------------------------------;
; calculate farmer WTA
;-----------------------------------------------------------------------------------;
to calculate-farmer-wta
  ; calculate WTA distribution
  let p-wta-list r-calculate-wta-distribution ;  list of lists of lists for normally distributed WTA (FSA type, AES, wta values)

  foreach range length p-g-aes-type-list
  [ i -> 
    ; ask open farmers
    ask farmers with [ item i v-open-to-aes-list = 1 ] [
      ifelse farmer-type? 
      [  
        ; draw individual WTA from respective list
        let t-farmer-type-num position p-fsa-spec [1 2 3 4 5]  ; get position of farmer fsa type
        let t-sublist1 item t-farmer-type-num p-wta-list  ; get list of normally distrubuted WTA for specific FSA type
        let t-ind-wta first item i t-sublist1  ; take first item from the list of distributed WTA for particular AES
        let t-sublist2 remove-item 0 item i t-sublist1   
        set p-accepted-payment-list replace-item i p-accepted-payment-list t-ind-wta  ; put accepted payment to a farmer's list
        set p-wta-list replace-item t-farmer-type-num p-wta-list (replace-item i t-sublist1 t-sublist2) ; exlude taken wta from p-wta-list  
                                                                                         
      ]
      [ ; farmer type is not included

        ; choose first item of ordered list and remove chosen item  
        let t-sublist1 p-wta-list  
        let t-ind-wta first item i t-sublist1    
        let t-sublist2 remove-item 0 item i t-sublist1    
        set p-accepted-payment-list replace-item i p-accepted-payment-list t-ind-wta 
        set p-wta-list replace-item i p-wta-list (replace-item i t-sublist1 t-sublist2) 
        set p-wta-list replace-item i p-wta-list t-sublist2 

      ]
    ]  
    
    ; set high WTA for farmers not open to specific AES
    ask farmers with [ item i v-open-to-aes-list = 0 ]
    [ set p-accepted-payment-list replace-item i p-accepted-payment-list 99999 ];
  ]
  
  ask farmers 
  [
    ; include access to advisory & contract characteristics in WTA
    set p-accepted-payment-list r-calculate-wta-diff p-accepted-payment-list

    ; replace negative WTA with 0
    set p-accepted-payment-list (map [ i -> ifelse-value (i < 0) [ 0 ] [ i ] ] p-accepted-payment-list)
  ]
end


;-----------------------------------------------------------------------------------;
; calculate WTA distribution
;-----------------------------------------------------------------------------------;
to-report r-calculate-wta-distribution
  let result [ ]

  ifelse farmer-type?
  [ 
    foreach range length [1 2 3 4 5] ; fsa
    [ x ->
      let filename-sd ""
      ; farm type
      let fsa-p-value x + 1
      
      ; in case some farmer types are not available
      ifelse any? farmers with [  p-fsa-spec = fsa-p-value ] 
      [
        let counter reduce [ [ a b ] -> (map + a b) ] [ v-open-to-aes-list ] of farmers with [  p-fsa-spec = fsa-p-value ] ; number of farmers who are open to every aes
        
        ; define file paths, different WTA files for different farmer-types
        set filename-sd (word "../data/wta/" i-g-wta-source "/WTA_P" fsa-p-value ".csv") 
        
        ; get mean and standard deviation for respective land type
        let t-sd-list item 1 csv:from-file filename-sd
        let t-mean-list item x p-g-wta-list ; p-g-wta-list - list of lists for base line scenario based on P categories ; t-mean-list - lists for baseline scenario for P category
        
        let t-wta-distribution []
        
        ; get normally distributed values for each AES, number of values depending on number of open farmers
        foreach range length p-g-aes-type-list
        [ i -> 
          let t-aes-random-normal (n-values (item i counter) [ k -> (random-normal item i t-mean-list item i t-sd-list) ]) ; list of normally distributed values for a particular aes 
          set t-wta-distribution lput t-aes-random-normal t-wta-distribution ; list of lists of normally distributed values for all 4 AES
        ]
          
        set result lput t-wta-distribution result ; list of lists of lists of normally distributed values for all 4 AES per FSA type (list FSA > list AES > list of normally distributed values)
      ]
      [
        set result lput [] result ; in case some farmer types are not available
      ]
    ]
  ]
  
  
  [ ; farmer type is not included
    
    let filename-sd ""
    let counter reduce [ [ a b ] -> (map + a b) ] [ v-open-to-aes-list ] of farmers ; number of farmers who are open to each aes
    
    
    ; define file paths
    set filename-sd (word "../data/wta/" i-g-wta-source "/WTA.csv") 
   
    ; get mean and standard deviation for respective land type
    let t-sd-list item 1 csv:from-file filename-sd
    let t-mean-list p-g-wta-list 
    let t-wta-distribution []
    
    ; get normally distributed values for each AES, number of values depending on number of open farmers
    foreach range length p-g-aes-type-list
    [ i -> 
      let t-aes-random-normal (n-values (item i counter) [ k -> (random-normal item i t-mean-list item i t-sd-list) ])
      set t-wta-distribution lput t-aes-random-normal t-wta-distribution
    ]
    set result t-wta-distribution
  ]
  
  report result
end


;-----------------------------------------------------------------------------------;
; calculate WTA results for each AES based on contract details and farm attributes
;-----------------------------------------------------------------------------------;
to-report r-calculate-wta-diff [ wta-list ]
  let results wta-list
  ifelse farmer-type? [
    
      let fsa-position p-fsa-spec - 1
      
      ; difference in WTA if advisory is available (depending on farmer)
      let t-wta-advisory-farmer-list (map [ x -> x * p-advisory + 1 ] (item fsa-position p-g-wta-advisory)) ; 0.05 * 0/1 + 1
      set results (map [ [ a b ] -> a * b ] results t-wta-advisory-farmer-list)
     
      ; difference in WTA based on AES characteristics for duration
      set results (map [ [ a b ] -> a * b ] results (item fsa-position p-g-wta-duration-list))
        
      ; difference in WTA based on AES characteristics for administrative efforts
      set results (map [ [ a b ] -> a * b ] results (item fsa-position p-g-wta-admin-list))
      
      ; round to two digits
      set results (map [ i -> precision i 2 ] results)
    
      ; reverse calculations if WTA = 99999, i.e. no adoption
      set results (map [ [ a b ] -> (ifelse-value a = 99999 [ 99999 ] [ b ]) ] wta-list results)
      
  ]
  
  [ ; farmer types not included
    
    ; difference in WTA if advisory is available (depending on farmer)
      let t-wta-advisory-farmer-list map [ x -> 1 + (x * p-advisory) ]  p-g-wta-advisory
      set results (map [ [ a b ] -> a * b ] results  t-wta-advisory-farmer-list)
   
      ; difference in WTA based on AES characteristics for duration
      set results (map [ [ a b ] -> a * b ] results p-g-wta-duration-list)
      
    
      ; difference in WTA based on AES characteristics for administrative efforts
      set results (map [ [ a b ] -> a * b ] results  p-g-wta-admin-list)
      
      ; round to two digits
      set results (map [ i -> precision i 2 ] results)
  
      ; reverse calculations if WTA = 99999, i.e. no adoption
      set results (map [ [ a b ] -> (ifelse-value a = 99999 [ 99999 ] [ b ]) ] wta-list results)
    
  ]
  report results
end



;-----------------------------------------------------------------------------------;
; farmers choose which contract to apply on which fields
;-----------------------------------------------------------------------------------;
to choose-suitable-contract
  ; order AES application based on the highest difference between exptected and offered payments
  let t-aes-decision-order (map [ [ a b c ] -> a * (b - c) ] p-accepted-aes-list p-g-offered-payment-list p-accepted-payment-list)
  
  ; check for each AES farmer accepts and is open to where to adopt AES
  while [ max t-aes-decision-order > 0 ]
  [
    ; get index of AES in list, remove that AES to prevent further decisions in same year
    ; if several conditions fulfill the selection criterion equally well, randomly
    ; choose one of them to apply first
    let t-which-aes one-of all-positions-max t-aes-decision-order 
    set t-aes-decision-order replace-item t-which-aes t-aes-decision-order 0 
    
    ; select fields on which no AES is already applied, update after previous decisions
    let possible-fields item t-which-aes v-suitable-fields-list 
    set v-suitable-fields-list replace-item t-which-aes v-suitable-fields-list 
    possible-fields with [ sum v-aes-list = 0 ] 
    
    ; select fields on which to put the selected AES (given through index 
    ; 'which-aes'), consider only AES for which enough area is available
    if any? item t-which-aes v-suitable-fields-list [ select-aes-fields t-which-aes ]
  ]
end


;-----------------------------------------------------------------------------------;
; select fields on which to put the selected AES
;-----------------------------------------------------------------------------------;
to select-aes-fields [ which-aes ]
  let application-list []
  let required-temp item which-aes p-envisioned-area-list
  let field-list sort item which-aes v-suitable-fields-list
  let total-suitable-area sum [ p-area ] of item which-aes v-suitable-fields-list
  set total-suitable-area precision total-suitable-area 2
  
  ; if available field size is smaller or equal to the envisioned area on which 
  ; farmer intends to apply AES: select all fields for AES
  ifelse length field-list = 1 OR total-suitable-area <= required-temp
  [ 
    
    set application-list field-list 
    set v-nr-aes-fields-list replace-item which-aes v-nr-aes-fields-list length field-list
    
  ]
  
  ; if available field size is larger than the envisioned area on which farmer
  ; intends to apply AES: choose parcels  with lowest soil quality
  ; if two fields with the same soil-quality, the smaller one is chosen
  [
      let sorted-fields [] 
     
      ; sort fields: choose patches with lowest soil quality
      ; if two fields with the same soil-quality, the smaller one is chosen
      set sorted-fields sort-by [ [a b] -> ([ p-soil-quality ] of a < [ p-soil-quality ] of b) or 
       ([ p-soil-quality ] of a = [ p-soil-quality ] of b and [ p-area ] of a < [ p-area ] of b) ] 
       (field-list)

      ; iterate though sorted fields and apply a scheme until envisioned area <= of total area where a farmer applied a scheme
      foreach sorted-fields
      [ i ->
     
      if ((empty? application-list) or (required-temp > sum [p-area] of (turtle-set application-list))) [ set application-list lput i application-list ]

      ]
      
      ; update number of aes fields
      set v-nr-aes-fields-list replace-item which-aes v-nr-aes-fields-list (length  application-list)
  ]
  
  ; apply the AES to the selected fields      
  let contract-area-temp 0
  ask turtle-set application-list
  [
    set v-aes-list replace-item which-aes v-aes-list 1
    set v-aes-hist-list replace-item which-aes v-aes-hist-list (item which-aes v-aes-hist-list + 1)
    
    ; create AES
    hatch-aes 1 
    [ 
      set v-aes-name item which-aes p-g-aes-type-list
      ; define AES properties
      setup-aes 
    ] 
    set contract-area-temp contract-area-temp + p-area
  ]
  set v-contract-area-list replace-item which-aes v-contract-area-list contract-area-temp
end

;-----------------------------------------------------------------------------------;
; create AES on selected field and set field and farmer related variables
;-----------------------------------------------------------------------------------;
to setup-aes 
  set v-aes-nr (ifelse-value 
    v-aes-name = "buffer-strips" [ 0 ]
    v-aes-name = "catch-crops" [ 1 ]
    v-aes-name = "grassland" [ 2 ]
    [ 3 ]) ; conversion
  
  set size 0
  set v-aes-contract-year 1 ; first year
  
  set v-aes-size [ p-area ] of myself
  let farmer-id [ p-owner-id ] of myself
  set v-aes-owner farmers with [ p-farmer-id = farmer-id ]
  set v-aes-field turtle-set myself
  
  ; color field according to AES adoption
  let aes-color (list (green - 1) (brown + 1) (sky + 1) (red + 2)) 
  let aes-field gis:find-one-feature p-g-landscape-dataset-shp "FIELD_ID" [ p-field-id ] of myself
  gis:set-drawing-color item v-aes-nr aes-color 
  gis:fill aes-field 2.0 
  gis:set-drawing-color black
  gis:draw aes-field 0.01
end
