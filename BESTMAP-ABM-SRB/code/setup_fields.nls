;-----------------------------------------------------------------------------------;
; create spatial distribution of parcels and farmers
;-----------------------------------------------------------------------------------;
to create-landscape
  
  ; define file paths
  let filename-fields ""
  let filename-projection ""
  
  set filename-fields "../data/landscape/field-file.shp"
  set filename-projection "../data/landscape/field-file.prj"
  
  ; define variable that contains field dataset including associated farmer ID
  ; IMPORTANT NOTE: Column names must be written in capital letters
  set p-g-landscape-dataset-shp gis:load-dataset filename-fields
  ; set the world projection 
  gis:load-coordinate-system filename-projection
  
  ; set envelope based on loaded files 
  gis:set-world-envelope (gis:envelope-of p-g-landscape-dataset-shp)
  
  ; create landscape from GIS input data
  create-individual-fields
end

;-----------------------------------------------------------------------------------;
; landscape creation
;-----------------------------------------------------------------------------------;
to create-individual-fields
  ask patches [ set pcolor white ]
  
  ; create fields with all attribute of interest
  foreach gis:feature-list-of p-g-landscape-dataset-shp 
  [ feature ->
    let centroid gis:location-of gis:centroid-of feature
    
    ; create field agent
    create-fields 1 
    [     
      set p-owner-id gis:property-value feature "OWNER_ID"
      set p-field-id gis:property-value feature "FIELD_ID"
      set p-land-use gis:property-value feature "LAND_USE"
      set p-area gis:property-value feature "AREA_HA"             
      set p-soil-quality gis:property-value feature "SOIL_Q"       
     
      
      ; starting point: no AES on fields 
      set v-aes-list [ 0 0 0 0 ]
      
      ; staring point: no AES history on fields, since there are no AES in Serbia
      set v-aes-hist-list [ 0 0 0 0 ]
      
      ; label field at center with owner ID
      set xcor item 0 centroid
      set ycor item 1 centroid
      set size 0
    ]
    
    ; draw polygon's borders
    gis:set-drawing-color black
    gis:draw feature 0.1
  ]
 
end
