# BESTMAP-ABM-SRB

BESTMAP-ABM-SRB is an agent-based model to determine the adoption and spatial allocation of agri-environmental schemes by individual farmers in the Bačka region in Serbia. With the model, the effect of different scenarios of policy design on patterns of adoption can be investigated. In particular, the model can be used to study the social-ecological consequences of agricultural policies at different spatial and temporal scales and, in combination with biophysical models, test the ecological implications of different designs of the EU’s Common Agricultural Policy.

The model was developed in the [BESTMAP project](https://bestmap.eu/) as one of five case study-specific models with the same core processes.

## Corresponding author: 
Nastasija Grujić (E-mail: <n.grujic@biosense.rs>)

Please get in touch if you have questions about the code or data.
