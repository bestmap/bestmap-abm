# BESTMAP-ABM
BESTMAP-ABM is an agent-based modelling suite to determine the adoption and spatial allocation of agri-environmental schemes by individual farmers in five case studies across Europe. With the models, the effect of different scenarios of policy design on patterns of adoption can be investigated. In particular, the models can be used to study the social-ecological consequences of agricultural policies at different spatial and temporal scales and, in combination with biophysical models, test the ecological implications of different designs of the EU’s Common Agricultural Policy.

This repository contains the model code for the Deliverable D4.1 'Agent-Based Models for each CS' of the [BESTMAP project](https://bestmap.eu/). The case study specific model versions are in the respective subfolders: 
- **BESTMAP-ABM-CZ**: case study South Moravia, Czech Republic
- **BESTMAP-ABM-DE**: case study Mulde region, Germany
- **BESTMAP-ABM-ES**: case study Catalonia, Spain
- **BESTMAP-ABM-SRB**: case study Bačka region, Serbia
- **BESTMAP-ABM-UK**: case study Humber region, UK

## Corresponding authors
- **BESTMAP-ABM-CZ**: Meike Will, Helmholtz Centre for Environmental Research - UFZ (E-mail: <meike.will@ufz.de>)
- **BESTMAP-ABM-DE**: Meike Will, Helmholtz Centre for Environmental Research - UFZ (E-mail: <meike.will@ufz.de>)
- **BESTMAP-ABM-ES**: Chunhui Li, University of Leeds (E-mail: <C.Li2@leeds.ac.uk>)
- **BESTMAP-ABM-SRB**: Nastasija Grujić, Nastasija Grujić, BioSense Institute (E-mail: <n.grujic@biosense.rs>)
- **BESTMAP-ABM-UK**: Chunhui Li, University of Leeds (E-mail: <C.Li2@leeds.ac.uk>)

Please get in touch if you have questions about the code or data.

## License
GPLv3, Copyright © 2023, the BESTMAP-ABM developers from Helmholtz Centre for Environmental Research - UFZ, University of Leeds and BioSense Institute. All rights reserved.

This license applies to the entire repository with specific property information in each subfolder.
