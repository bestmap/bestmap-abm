; Define social network that has influence on knowledge about AES
; options are 'neighbors' when spatial neighbors are asked or 
; FSA when farmers with the same type (specialization and economic size)
; are considered
to setup-social-network
  if i-g-social-network-type = "none" 
  [ 
    ask farmers [ set v-social-network no-turtles ]
  ]
  
  ; get social network based on spatial neighbors (field position)
  if i-g-social-network-type = "neighbors" 
  [
    ; initialize helper variable
    ask farmers [ set v-neighbor? false ] 
    
    ask farmers 
    [ 
      ask p-property-set
      [
        ask fields in-radius i-g-social-network-radius
        [
          let t-owner-id p-owner-id
          ask farmers with [ p-farmer-id = t-owner-id ]
          [
            set v-neighbor? true
          ]
        ]
      ]
      set v-social-network other farmers with [ v-neighbor? ]
      ask v-social-network [ set v-neighbor? false ]
      set v-neighbor? false
    ]
  ]
  
  ; get social network based on specialization and economic size of farm
  if i-g-social-network-type = "FSA"
  [
    ask farmers
    [
      set v-social-network other farmers with 
      [ p-fsa-size = [ p-fsa-size ] of myself AND 
        p-fsa-spec = [ p-fsa-spec ] of myself ]
    ]
  ]
end