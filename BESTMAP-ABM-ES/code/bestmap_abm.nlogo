extensions
[
  gis
  csv
  matrix
]

;-----------------------------------------------------------------------------------;
; Include external files
;-----------------------------------------------------------------------------------;
__includes
[
  "setup_fields.nls"
  "setup_farmers.nls"
  "setup_WTA.nls"
  "update_world.nls"
  "check_openness.nls"
  "social_network.nls"
  "select_fields.nls"
  "deliberation.nls"
  "helper.nls"
]

;-----------------------------------------------------------------------------------;
; Global variables, variables that are commented are defined via the interface
;-----------------------------------------------------------------------------------;
globals
[
  ;-----------------------------------------------------------------------------------;
  ; Model settings
  ;-----------------------------------------------------------------------------------;
  ; i-g-set-seed?                     ; if true, seed is set manually via interface, for nlrx set-seed? false
  ; i-g-model-seed                    ; manually set seed when set-seed? true
  ; i-g-years                         ; number of years simulated

  ;-----------------------------------------------------------------------------------;
  ; GIS input
  ;-----------------------------------------------------------------------------------;
  p-g-landscape-dataset-shp       ; shp input file (field-file.shp) with GIS dataset

  ;-----------------------------------------------------------------------------------;
  ; AES input
  ;-----------------------------------------------------------------------------------;
  ; lists to combine contract details for all AES as defined via interface/input
  p-g-aes-type-list               ; list with names of all AES
  p-g-duration-list               ; list with contract duration of all AES
  p-g-bureaucracy-list            ; list with administrativ effort of all AES
  p-g-offered-payment-list        ; list with offered payment levels for all AES
  p-g-area-min-list               ; list with minimum plot size for all AES

  ; individual variables for AES (defined via interface/input)
  ; i-g-area-min-buffer-strips    ; minimum plot size for buffer strips
  ; i-g-area-min-catch-crops      ; minimum plot size for catch crops
  ; i-g-area-min-grassland        ; minimum plot size for maintaining grassland
  ; i-g-area-min-conversion       ; minimum plot size for conversion of arable land to grassland

  ; i-g-duration-buffer-strips    ; contract duration buffer strips
  ; i-g-duration-catch-crops      ; contract duration catch crops
  ; i-g-duration-grassland        ; contract duration maintaining grassland
  ; i-g-duration-conversion       ; contract duration conversion of arable land to grassland

  ; i-g-bureaucracy-buffer-strips ; contract details bureaucracy buffer strips
  ; i-g-bureaucracy-catch-crops   ; contract details bureaucracy catch crops
  ; i-g-bureaucracy-grassland     ; contract details bureaucracy maintaining grassland
  ; i-g-bureaucracy-conversion    ; contract details bureaucracy conversion of arable land to grassland

  ; i-g-payment-buffer-strips     ; offered payment buffer strips
  ; i-g-payment-catch-crops       ; offered payment catch crops
  ; i-g-payment-grassland         ; offered payment maintaining grassland
  ; i-g-payment-conversion        ; offered payment conversion of arable land to grassland

  ; i-g-site-selection            ; order of site selection for accepted schemes

  ;-----------------------------------------------------------------------------------;
  ; Discrete choice experiment input
  ;-----------------------------------------------------------------------------------;
  p-g-WTA-list                    ; list of lists for WTA for different AES
  p-g-WTA-advisory-list           ; list of difference in WTA when advisory is available for different AES

  p-g-area-list                   ; list of lists with area on which farmers would apply individual AES
  p-g-area-advisory-list          ; list of difference in area when advisory is available for different AES
  p-g-advisory-list               ; list of whether free adivosry service is offered for the four AES
  ;-----------------------------------------------------------------------------------;
  ; Farmers' attitude
  ;-----------------------------------------------------------------------------------;
  ; i-g-access-to-advisory        ; probability that a farmer has access to advisory
  ; i-g-social-network-type       ; choose which other farmers have an influence (spatial neighbors, FSA)
  ; i-g-social-network-radius     ; radius in which other farmers are considered if influence is from spatial neighbors
  ; i-g-prob-open-experience      ; probability that farmer with prior experience is open
  ; i-g-prob-open-advisory        ; probability that farmer with access to advisory is open
  ; i-g-prob-open-social          ; probability that farmer with positive social influence is open
  p-g-prob-intrinsic-open-list    ; probability that farmer is intrinsically open (list of all FSAs and AES)

  ;-----------------------------------------------------------------------------------;
  ; Knapsack algorithm to select fields for AES
  ;-----------------------------------------------------------------------------------;
  v-g-knapsack-results-temp       ; temporary list variable that stores knapsack results

  ;--------------------------------------------------------------------;
  ; Land use code for Catalonia
  ;--------------------------------------------------------------------;
  p-g-arable-code-list           ;list of arable land codes
  p-g-grassland-code-list        ;list of grassland codes
  p-g-horticulture-code-list     ; list of horticulture
  p-g-efa-code-list              ; list of codes for EFA crops and zones

  ;-----------------------------------;
  ; For multiple runs
  ;------------------------------------;
  p-g-adoption-rates
  p-g-record-seeds
]

;-----------------------------------------------------------------------------------;
; Breeds
;-----------------------------------------------------------------------------------;
breed [ farmers farmer ]
breed [ fields field ]
breed [ aes a-aes ]

farmers-own
[
  ;-----------------------------------------------------------------------------------;
  ; Input
  ;-----------------------------------------------------------------------------------;
  p-farm-area                    ; size of farm given by sum of field sizes
  p-farmer-id 		               ; farmer ID read from input file, data format depending on input format
  p-fsa-spec                     ; farm specialization (FSA), read from input file
  p-fsa-size                     ; farm size (FSA) classification, read from input file
  p-eco                          ; organic farming practice (1/0)
  p-advisory			               ; if a farmer has access to advisory (1/0)
  p-property-set                 ; agentset of all fields a farmer owns
  p-gis-feature-list             ; GIS feature list of all fields a farmer owns
  p-stats-farm-area-list         ; a list of farm areas stats - total area, arable area, grassland area and horticulture area
  ;-----------------------------------------------------------------------------------;
  ; Attitude and openness
  ;-----------------------------------------------------------------------------------;
  v-prior-experience-list        ; list of farmer's prior experience with each AES (1/0)
  v-social-network               ; agentset of farmers that influence the farmer (neighbors and same FSA)
  v-neighbor?                    ; helper variable for setting up the social-network
  v-open-to-aes-list             ; list of AES the farmer is open to (1/0)
  p-prob-intrinsic-open-list     ; farmer's probability of being intrinsically open to an AES

  ;-----------------------------------------------------------------------------------;
  ; Willingness to accept
  ;-----------------------------------------------------------------------------------;
  p-accepted-payment-list        ; list of accepted payment level calculated based on AES contract details and farmer characteristics
  p-accepted-aes-list            ; list indicating for each AES whether offered payment level is larger or equal compared to the accepted payment level (0/1)
  p-envisioned-area-list         ; area percentages of the total farm area a farmer is willing to use for each AES
  p-envisioned-area-ha-list      ; area in ha of a farmer's envisioned area
  ;-----------------------------------------------------------------------------------;
  ; AES fields
  ;-----------------------------------------------------------------------------------;
  v-suitable-fields-list         ; list of agentset of all fields suitable for each AES
  v-contract-area-list           ; list of total area on which farmer has AES contracts for each AES
  v-nr-aes-fields-list           ; list of number of AES contracts for each AES
]

fields-own
[
  p-owner-id                     ; owner ID (farmer)
  p-field-id                     ; field ID
  p-land-use                     ; land use on the field
  p-area                         ; size of the field
  p-EFA                          ; EFA status of the field (1/0)
  p-soil-quality                 ; soil quality of the field
  v-aes-list                     ; list of AES currently applied on the field
  v-aes-hist-list                ; list of history of AES previously applied to the field
]

aes-own
[
  v-aes-name                     ; name of AES (buffer strip, catch crops, grassland, conversion)
  v-aes-nr                       ; number of AES type (0: buffer strip, 1: catch crops, 2: grassland, 3: conversion)
  v-aes-contract-year            ; current duration of AES contract (in years) 				
  v-aes-size		                 ; size of AES contract (field size)
  v-aes-owner                    ; farmer who signed contract for that AES (agentset)
  v-aes-field     	             ; field on which AES is applied (agentset)
]

;-----------------------------------------------------------------------------------;
; Setup
;-----------------------------------------------------------------------------------;
to setup
  clear-all
  reset-timer

  ; set the model random seed - only necessary when using locally,
  ; for nlrx set-seed? false
  if i-g-set-seed? [ random-seed i-g-model-seed ]

  ; create the landscape (fields) by loading GIS data (setup_fields.nls)
  create-landscape

  ; load WTA and area specifications according to AES properties (setup_WTA.nls)
  setup-DCE-specifics

  ; setup farmers: load input file and define properties (setup_farmers.nls)
  setup-farmers

  reset-ticks
  show (word "Setup time: " timer)
end

;-----------------------------------------------------------------------------------;
; Go
;-----------------------------------------------------------------------------------;
to go
  reset-timer

  ; stop if simulated time span is reached
  if (ticks > i-g-years - 1) [ stop ]

  ; Step 0: Update the world
  update-world

  ; Step 1: Openness to specific AES (check_openness.nls)
  check-openness-to-aes

  ; Step 2: Select suitable fields (select_fields.nls)
  select-suitable-fields

  ; Step 3: Deliberation (deliberation.nls)
  ; check for each farmer and AES whether offered-payment is high enough and
  ; choose select fields where AES should be adopted
  deliberate-aes-decision

  tick
  show (word "Time tick " ticks ": " timer)
end

to go-x-times
  ;initialise the global vairables
  set p-g-adoption-rates []
  set p-g-record-seeds []

  ;initialise the local variables
  let record-errors []
  let farmer-adoption-rate []
  let record-seeds []
  let aes-pos 0  ; which AES to record

  let i 0
  ;let m-start date-and-time
  while [i < i-g-times] [
    setup
    go
    set farmer-adoption-rate fput report-farm-adoption-rate-list farmer-adoption-rate
    set record-seeds fput p-g-record-seeds record-seeds
    set i i + 1
  ]

  set p-g-adoption-rates farmer-adoption-rate

  ;calculate average output of the runs
  set p-g-adoption-rates report-mean-list-of-list farmer-adoption-rate 5
  show (word "The average adoption rates: " p-g-adoption-rates)

  set p-g-record-seeds record-seeds
  ;write-model-metrics mean record-errors mean farmer-adoption-rate list-to-R-vector record-errors list-to-R-vector farmer-adoption-rate list-to-R-vector p-g-record-seeds
  ;let m-end date-and-time
  ;show (word "Start: " m-start "; End: " m-end ". -----Done!")
  stop
end




	
@#$#@#$#@
GRAPHICS-WINDOW
195
10
963
773
-1
-1
5.033113
1
10
1
1
1
0
1
1
1
0
150
0
149
0
0
1
ticks
30.0

BUTTON
10
160
65
193
setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
20
595
170
655
i-g-access-to-advisory
0.8
1
0
Number

INPUTBOX
980
640
1135
700
i-g-payment-buffer-strips
661.0
1
0
Number

INPUTBOX
1140
640
1295
700
i-g-payment-catch-crops
162.0
1
0
Number

INPUTBOX
1300
640
1455
700
i-g-payment-grassland
168.0
1
0
Number

INPUTBOX
1460
640
1615
700
i-g-payment-conversion
367.0
1
0
Number

CHOOSER
980
590
1135
635
i-g-bureaucracy-buffer-strips
i-g-bureaucracy-buffer-strips
"low" "medium" "high"
1

CHOOSER
1460
590
1615
635
i-g-bureaucracy-conversion
i-g-bureaucracy-conversion
"low" "medium" "high"
1

CHOOSER
1140
590
1295
635
i-g-bureaucracy-catch-crops
i-g-bureaucracy-catch-crops
"low" "medium" "high"
1

CHOOSER
1300
590
1455
635
i-g-bureaucracy-grassland
i-g-bureaucracy-grassland
"low" "medium" "high"
1

BUTTON
67
160
122
193
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
10
65
130
125
i-g-model-seed
500.0
1
0
Number

SWITCH
10
30
130
63
i-g-set-seed?
i-g-set-seed?
1
1
-1000

INPUTBOX
980
705
1135
765
i-g-area-min-buffer-strips
0.5
1
0
Number

INPUTBOX
1140
705
1295
765
i-g-area-min-catch-crops
0.5
1
0
Number

INPUTBOX
1300
705
1455
765
i-g-area-min-grassland
0.5
1
0
Number

INPUTBOX
1460
705
1615
765
i-g-area-min-conversion
0.5
1
0
Number

INPUTBOX
125
380
185
440
i-g-years
1.0
1
0
Number

CHOOSER
20
715
170
760
i-g-social-network-type
i-g-social-network-type
"none" "neighbors" "FSA"
0

INPUTBOX
20
760
170
820
i-g-social-network-radius
5.0
1
0
Number

CHOOSER
10
315
157
360
landscape
landscape
"samples_ES" "ES_2018"
0

PLOT
1270
10
1555
230
The Number of AES Contracts
tick
AES Contracts
0.0
20.0
0.0
120.0
true
true
"" ""
PENS
"Total" 1.0 0 -16777216 true "" "plot count aes"
"BF" 1.0 0 -12087248 true "" "plot count aes with [v-aes-name = \"buffer-strips\"]"
"CC" 1.0 0 -5207188 true "" "plot count aes with [v-aes-name = \"catch-crops\"]"
"MG" 1.0 0 -11033397 true "" "plot count aes with [v-aes-name = \"grassland\"]"
"CNV" 1.0 0 -1604481 true "" "plot count aes with [v-aes-name = \"conversion\"]"

PLOT
980
10
1260
230
FSA Farms
FSA P1 - P5
No. of Farmers
1.0
20.0
0.0
100.0
true
false
"" ""
PENS
"FSA" 0.5 1 -16777216 true "" "histogram [p-fsa-spec] of farmers\nset-plot-x-range 1 6"

PLOT
1270
235
1555
455
AES Contracts in FSA Farms
ticks
AES Contracts
0.0
20.0
0.0
80.0
true
true
"" ""
PENS
"P1" 1.0 0 -12087248 true "" "plot-fsa-aes-uptake 1"
"P2" 1.0 0 -5207188 true "" "plot-fsa-aes-uptake 2"
"P3" 1.0 0 -12895429 true "" "plot-fsa-aes-uptake 3"
"P4" 1.0 0 -2674135 true "" "plot-fsa-aes-uptake 4"
"P5" 1.0 0 -13345367 true "" "plot-fsa-aes-uptake 5"

TEXTBOX
985
510
1135
528
BS: buffer strips (flower)
11
0.0
1

TEXTBOX
1150
510
1300
528
CC: catch-crops (plant)
11
0.0
1

TEXTBOX
1310
510
1460
536
MG: maintaining grassland (dot)
11
0.0
1

TEXTBOX
1470
510
1615
551
CNV: conversion of arable land to grassland (x)
11
0.0
1

TEXTBOX
840
615
870
655
■
40
45.0
1

TEXTBOX
840
639
870
684
■
40
55.0
1

TEXTBOX
840
674
870
720
■
40
115.0
1

TEXTBOX
880
639
935
657
arable land
11
0.0
1

TEXTBOX
880
669
930
687
grassland
11
0.0
1

TEXTBOX
880
699
940
717
horticulture
11
0.0
1

SWITCH
605
820
750
853
calculate-openness?
calculate-openness?
0
1
-1000

TEXTBOX
840
705
865
750
■
40
5.0
1

TEXTBOX
880
730
1030
748
other
11
0.0
1

PLOT
1565
10
1875
230
Fraction of Farms Adopting AES
NIL
NIL
0.0
20.0
0.0
0.5
true
true
"" ""
PENS
"Total" 1.0 0 -16777216 true "" "plot count farmers with [ sum v-contract-area-list > 0 ] / count farmers"
"BF" 1.0 0 -12087248 true "" "plot count farmers with [ item 0 v-contract-area-list > 0 ] / count farmers"
"CC" 1.0 0 -5207188 true "" "plot count farmers with [ item 1 v-contract-area-list > 0 ] / count farmers"
"MG" 1.0 0 -11033397 true "" "plot count farmers with [ item 2 v-contract-area-list > 0 ] / count farmers"
"CNV" 1.0 0 -1604481 true "" "plot count farmers with [ item 3 v-contract-area-list > 0 ] / count farmers"

PLOT
1565
235
1875
455
Fraction of AES Area (in Total Farm Area)
NIL
NIL
0.0
20.0
0.0
0.2
true
true
"" ""
PENS
"Total" 1.0 0 -16777216 true "" "plot sum [p-area] of fields with [ sum v-aes-list > 0 ] / sum [p-area] of fields\n"
"BF" 1.0 0 -12087248 true "" "plot sum [p-area] of fields with [ item 0 v-aes-list > 0 ] / sum [p-area] of fields"
"CC" 1.0 0 -5207188 true "" "plot sum [p-area] of fields with [ item 1 v-aes-list > 0 ] / sum [p-area] of fields"
"MG" 1.0 0 -11033397 true "" "plot sum [p-area] of fields with [ item 2 v-aes-list > 0 ] / sum [p-area] of fields"
"CNV" 1.0 0 -1604481 true "" "plot sum [p-area] of fields with [ item 3 v-aes-list > 0 ] / sum [p-area] of fields"

INPUTBOX
20
475
172
535
i-g-prob-open-experience
0.95
1
0
Number

INPUTBOX
20
535
172
595
i-g-prob-open-advisory
0.5
1
0
Number

INPUTBOX
20
655
170
715
i-g-prob-open-social
0.1
1
0
Number

SWITCH
755
820
900
853
distributed-WTA?
distributed-WTA?
0
1
-1000

CHOOSER
210
815
370
860
i-g-site-selection
i-g-site-selection
"highest-payment" "highest-payment-diff" "highest-payment-ratio" "largest-area"
2

CHOOSER
980
540
1135
585
i-g-duration-buffer-strips
i-g-duration-buffer-strips
1 5 10
1

CHOOSER
1140
540
1295
585
i-g-duration-catch-crops
i-g-duration-catch-crops
1 5 10
1

CHOOSER
1300
540
1455
585
i-g-duration-grassland
i-g-duration-grassland
1 5 10
1

CHOOSER
1460
540
1615
585
i-g-duration-conversion
i-g-duration-conversion
1 5 10
1

MONITOR
980
285
1215
330
Farm adoption rates
report-farm-adoption-rate-list
17
1
11

BUTTON
10
245
97
278
go-x-times
go-x-times
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
105
220
175
280
i-g-times
50.0
1
0
Number

TEXTBOX
15
10
165
28
Set seeds:
14
0.0
1

TEXTBOX
15
140
165
158
A single run:
14
0.0
1

BUTTON
127
160
182
193
NIL
stop
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

TEXTBOX
15
220
165
238
Multiple runs:
14
0.0
1

TEXTBOX
10
295
160
313
Farms and fields:
14
0.0
1

TEXTBOX
10
380
160
398
Duration of a run:
14
0.0
1

TEXTBOX
210
790
360
808
Farmers' reasoning:
14
0.0
1

TEXTBOX
605
795
755
813
For testing purposes:
14
0.0
1

TEXTBOX
985
475
1135
493
Offered AES:
14
0.0
1

TEXTBOX
20
445
170
463
Openness:
14
0.0
1

SWITCH
980
770
1135
803
i-g-advisory-BS?
i-g-advisory-BS?
1
1
-1000

SWITCH
1140
770
1287
803
i-g-advisory-CC?
i-g-advisory-CC?
1
1
-1000

SWITCH
1300
770
1455
803
i-g-advisory-MG?
i-g-advisory-MG?
1
1
-1000

SWITCH
1460
770
1612
803
i-g-advisory-CVN?
i-g-advisory-CVN?
1
1
-1000

@#$#@#$#@
## WHAT IS IT?

The model is simulating Catalonia farmers' decision-making process on AES adoptions. It focuses on four types of AES: buffer strips (BS), cover crops (CC), grassland management (GM) and conversion of arable land to grassland (CVN). The model can be used to study different AES designs' impact on farm adoptions at macro level.

## HOW IT WORKS

The agents in the model include farmers, fields and AES contracts. Each farmer agent has a set of field agents. A farmer agent decides to take up an AES based on (1) whether the farmer agent is open to the AES; (2) whether the farmer agent has suitable fields to implement the AES; and (3) whether the offered payment is higher than the amount of money that the farmer is willing to accept (WTA). When three conditions are satisfied, the farmer agent will choose a field to sign up the AES, which generates an AES contract agent in the model.

At the macro level, we look at farmers' adoption rate of the four types of AES in the whole region.


## HOW TO USE IT

The model interface has different sections containing various variables to set up before running the model and display the model output.

(1) "Set seeds" section:  
<b/>i-g-set-seed?</b> is used to decide whether the seed of a run is manually set up or not. if it’s true, the seed is set manually via interface. Otherwise, the seed is set automatically during the model run.  
 <b/>i-g-model-seed</b> is an user-input for manually setting the seed. The variable will be in use when i-g-set-seed? is true.
(2) “A single run” section:
The functions include:  “setup” function initialises the model, “go” is to run the model and “stop” is to stop a run of the model.
(3) “Multiple runs” section:
This section is implemented for running multiple times and investigating the average adoption rates of the multiple runs. This module is designed to get the average outputs directly rather than setting up experiments in Behaviour Space. The number of runs is set by <b/>i-g-times</b>. When using "go-x-times" function for simulations, the result will be displayed at Comment Center at the end, which includes the average adoption rates of BS, CC, GM, CVN and the total farm adoption rates. 
(4) “Farms and fields” section:
In this section, a specific landscape is chosen. The available choices are a farms and fields’ sample and the farms and fields of the whole Catalonia region. Please note that the Catalonia region includes more than a million fields and using the farms and fields of the whole region as landscape requires a huge computer memory.
(5) “Duration of a run” section:
<b/>i-g-years</b> defines the duration of a simulation run. By default, it is set to be 1.
(6) “Openness” section:
<b/>i-g-prob-open-experience</b> is the probability that a farmer with prior experience is open;
<b/>i-g-prob-open-advisory</b> is the probability that a farmer with access to advisory is open;
<b/>i-g-access-to-advisory</b> is the probability that a farmer has access to advisory;
i-g-prob-open-social is the probability that a farmer with positive social influence is open;
<b/>i-g-social-network-type</b> defines which other farmers have an influence, including no other farmers, spatial neighbours, other farmers belong to the same FSA and other farmers in the same village);
<b/>i-g-social-network-radius</b> defines the radius in which other farmers are considered if influence is from spatial neighbours.
(7) “Farmers’ reasoning” section
<b/>i-g-site-selection</b> is farmers’ preference when they determine which accepted scheme is the most profitable.
<b/>Knapsack-optimisation?</b> is to choose whether in a simulation farmers go through an optimisation process considering soil quality and field size in order to find the best fields for AES adoptions. If it is true, the optimisation is switched on for each farmer agent.
(8) “Offered AES” section
This section includes the input parameters of the modelled AES. Four aspects of AES characteristics are included: the duration of a contract (<b/>i-g-duration-AES*</b>), the bureaucracy level(<b/>i-g-bureaucracy-AES*</b>), the offered payment (<b/>i-g-payment-AES*</b>) and the minimal plot size to implement the AES on (<b/>i-g-area-min-AES*</b>). Note, AES* can be any of the four selected AES.
(9) “For testing purposes” section
This section of variables are designed for running the test of the model in order to explore farmer adoptions in two scenarios: 1) when <b/>calculate-openness?</b> is off, the farmer agents skip the first decision-making step --- openness; 2) when <b/>distributed-WTA?</b> is turned off, the WTA values of the farmer agents are the same, instead of normally distributed. By default, these two variables are on.

## THINGS TO NOTICE

After each run of the simulation, the statistics of the run is displayed in the monitors, which include farm adoption rates (in sequence of BS, CC, GM, CVN and total),  the distribution of FSA farms in the farm population, the number of AES contracts, the number of AES contracts in different FSA farms, the fraction of farms that adopt AES, the fraction of AES area in the region.

## THINGS TO TRY

To test farmers’ adoptions in different scenarios, you could try out the variables in the “Offered AES” section. 
You could test the variables in the “Farmers’ reasoning'' section and check how different ways of profit calculation and knapsack optimisation influence the farm-level adoptions  and field-level adoptions (i.e. AES area). 
You could also try out the variables in the “For testing purposes” section and check how openness step and WTA influence the model outputs.

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)


## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

The BESTMAP-ABM-ES is a member of the BESTMAP model suite. Five ABMs are developed in the model suite for five case studies in the Czech Republic, Germany, Serbia, Spain and the UK. 

## CREDITS AND REFERENCES

The model is developed under the BESTMAP ABM team’s effort, including Chunhui Li, Meike Will, Nastasija Grujić, Jiaqi Ge and Birgit Müller.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.2.2
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="experiment" repetitions="1" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>count turtles</metric>
    <enumeratedValueSet variable="i-g-payment-grassland">
      <value value="168"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-area-min-grassland">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-bureaucracy-conversion">
      <value value="&quot;medium&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-payment-buffer-strips">
      <value value="300"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-prob-open-advisory">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-site-selection">
      <value value="&quot;highest-payment&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knapsack-optimisation?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="calculate-openness?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-area-min-catch-crops">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-payment-catch-crops">
      <value value="162"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-prob-open-experience">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-duration-buffer-strips">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-area-min-conversion">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-bureaucracy-buffer-strips">
      <value value="&quot;medium&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-access-to-advisory">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-duration-catch-crops">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="distributed-WTA?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-years">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-social-network-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-set-seed?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="landscape">
      <value value="&quot;samples_ES&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-bureaucracy-grassland">
      <value value="&quot;medium&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-times">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-prob-open-social">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-social-network-type">
      <value value="&quot;none&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-model-seed">
      <value value="500"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-payment-conversion">
      <value value="300"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-bureaucracy-catch-crops">
      <value value="&quot;medium&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-area-min-buffer-strips">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-duration-conversion">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="i-g-duration-grassland">
      <value value="5"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
1
@#$#@#$#@
