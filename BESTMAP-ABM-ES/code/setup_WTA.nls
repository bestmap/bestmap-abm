;-----------------------------------------------------------------------------------;
; load WTA and envisioned area specifications according to AES properties
;-----------------------------------------------------------------------------------;
; read the AES properties (defined via the interface/input) and combine the information 
; in the resulting WTA/area, only including AES characteristics, i.e. duration and 
; bureaucracy and no information about farm properties (advisory, size, soil quality)
to setup-DCE-specifics
  ; define AES properties based on interface/input
  setup-aes-constants
  
  ; load WTA 
  let filename-WTA "../data/DCE/mockupdata_WTA.csv"
  set p-g-WTA-list r-calculate-DCE filename-WTA
  
  ; load envisioned area
  let filename-area "../data/DCE/mockupdata_area.csv"
  set p-g-area-list r-calculate-DCE filename-area

end

;-----------------------------------------------------------------------------------;
; define AES properties based on input (interface)
;-----------------------------------------------------------------------------------;
to setup-aes-constants
  ; collect names of AES in list
  set p-g-aes-type-list ["buffer-strips" "catch-crops" "grassland" "conversion"]
  
  ; combine input for contract attributes (defined via interface/input) in lists
  ; combine contract length input (1/5/10 years)
   set p-g-duration-list
  (list i-g-duration-buffer-strips i-g-duration-catch-crops i-g-duration-grassland i-g-duration-conversion)
  ;show (word "p-g-duration-list = " p-g-duration-list )
  
  ; combine strings for bureaucracy input (low/medium/high)
  let t-bureaucracy-string-list 
  (list i-g-bureaucracy-buffer-strips i-g-bureaucracy-catch-crops i-g-bureaucracy-grassland i-g-bureaucracy-conversion)
  ; translate string input for bureaucracy into numerical values (0: low, 1: medium, 2: high)
  set p-g-bureaucracy-list 
  (map [ x -> (ifelse-value x = "low" [ 0 ] x = "medium" [ 1 ] [ 2 ]) ] t-bureaucracy-string-list)
  
  ; combine offered payments (defined via interface/input) in list
  set p-g-offered-payment-list 
  (list i-g-payment-buffer-strips i-g-payment-catch-crops i-g-payment-grassland i-g-payment-conversion)
  
  ; combine minimum plot size (defined via interface/input) in list
  set p-g-area-min-list 
  (list i-g-area-min-buffer-strips i-g-area-min-catch-crops i-g-area-min-grassland i-g-area-min-conversion)
  
  let t-advisory-list (list i-g-advisory-BS? i-g-advisory-CC? i-g-advisory-MG? i-g-advisory-CVN?)
  set p-g-advisory-list (map [i -> ifelse-value (item i t-advisory-list = true) [ 1 ] [ 0 ]] [0 1 2 3])
  ;show (word "p-g-advisory-list = " p-g-advisory-list)
end

;-----------------------------------------------------------------------------------;
; calculate WTA/area based on specific AES characteristics for duration and bureaucracy
;-----------------------------------------------------------------------------------;
to-report r-calculate-DCE [ filename ]
  ; read csv input file (filename)
  ; WTA/area for baseline contract (1 year, low bureaucracy, no advisory)
  
  let t-DCE-list []
  
  set t-DCE-list item 0 csv:from-file filename
  ;show (word "t-DCE-list: " t-DCE-list)
  
  ; translate string input for contract length into numerical values (0: 1 year, 1: 5 years, 2: ten years)
  let t-duration-list-dummy
  (map [ x -> (ifelse-value x = 1 [ 0 ] x = 5 [ 1 ] [ 2 ]) ] p-g-duration-list)

  
  ; difference (% of the baseline) in WTA/area for contract length options (1 or 10 years) different to baseline (5 years)
  let t-DCE-duration-one (lambda-multiply-list  item 0 item 1 csv:from-file filename t-DCE-list)
  ;show (word "t-DCE-duration-one = " t-DCE-duration-one)
  let t-DCE-duration-ten (lambda-multiply-list  item 0 item 2 csv:from-file filename t-DCE-list)
  ;show (word "t-DCE-duration-ten = " t-DCE-duration-ten)
 
  
  let t-DCE-duration-list (list t-DCE-duration-one [ 0 0 0 0] t-DCE-duration-ten)
  ; calculate difference in WTA based on contract length options
  let t-DCE-duration-specific (map [ [ a b ] -> item a item b t-DCE-duration-list ] [ 0 1 2 3 ]  t-duration-list-dummy) 

  ; difference in WTA/area for bureaucracy options (low, high) different to baseline (=medium)
  let t-DCE-bureaucracy-low item 0 item 4 csv:from-file filename 
  let t-DCE-bureaucracy-high item 0 item 5 csv:from-file filename
  let t-DCE-bureaucracy-list (list t-DCE-bureaucracy-low 0 t-DCE-bureaucracy-high)
  
  ; calculate difference in WTA based on bureaucracy options
  let t-DCE-bureaucracy-rate-list (map [ x -> item x t-DCE-bureaucracy-list ] p-g-bureaucracy-list)
  ;show (word "t-DCE-bureaucracy-specific = " t-DCE-bureaucracy-rate-list)
  let t-DCE-bureaucracy-specific (map [ [ a b ] -> a * b ] t-DCE-bureaucracy-rate-list t-DCE-list)
  ;show (word "t-DCE-bureaucracy-specific-t = " t-DCE-bureaucracy-specific)
  
  ; calculate difference in WTA based on whether free advisory support is offered or not
  let t-DCE-advisory-list (lambda-multiply-list  item 0 item 3 csv:from-file filename t-DCE-list)
  set t-DCE-advisory-list (map [ [ a b ] -> a * b ] t-DCE-advisory-list p-g-advisory-list)
  ;show (word "t-DCE-advisory-list = " t-DCE-advisory-list)
  
  ; combine baseline WTA and difference due to AES specifications (i.e., base WTA, duration, bureaucray and advisory)
  report (map [ [ a b c d] -> a + b + c + d] t-DCE-list t-DCE-duration-specific t-DCE-bureaucracy-specific t-DCE-advisory-list)
end


