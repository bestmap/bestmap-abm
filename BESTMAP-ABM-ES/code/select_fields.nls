;-----------------------------------------------------------------------------------;
; Step 2: Select suitable fields
;-----------------------------------------------------------------------------------;
to select-suitable-fields
  ask farmers 
  [ 
    ; derive suitable fields from property set
    set v-suitable-fields-list p-property-set 
    
    ; select fields without EFA
    set v-suitable-fields-list v-suitable-fields-list with [ p-EFA = 0 ]

    ; select fields which are suitable for AES adoption according to land-use
    set v-suitable-fields-list r-suitable-fields-land-use v-suitable-fields-list
    
    ; select fields which are suitable for AES adoption according to minimum size
    set v-suitable-fields-list r-suitable-fields-size v-suitable-fields-list
  ]
end

;-----------------------------------------------------------------------------------;
; select fields which are suitable for AES adoption according to land-use
;-----------------------------------------------------------------------------------;
to-report r-suitable-fields-land-use [ possible-field-agentset ]
  let suitable-fields-list []
  ; buffer strips: possible on arable fields - hypothetical for ES
  set suitable-fields-list lput possible-field-agentset 
  with [member? p-land-use p-g-arable-code-list] suitable-fields-list
  
  ; catch crops - mostly applied on horticulture fields in data
  set suitable-fields-list lput possible-field-agentset 
  with [member? p-land-use p-g-horticulture-code-list] suitable-fields-list
  
  ; maintaining grassland: possible on grassland
  set suitable-fields-list lput possible-field-agentset 
  with [member? p-land-use p-g-grassland-code-list] suitable-fields-list
  
  ; converting arable land to grassland: possible on arable land - hypothetical for ES
  set suitable-fields-list lput possible-field-agentset 
  with [member? p-land-use p-g-arable-code-list] suitable-fields-list
  
  report suitable-fields-list
end

;-----------------------------------------------------------------------------------;
; select fields which are suitable for AES adoption according to minimum size
;-----------------------------------------------------------------------------------;
to-report r-suitable-fields-size [ possible-field-agentset ]
  let r-suitable-fields-list []
  
  ; loop through all AES (buffer strips, catch crops, maintaining grassland, 
  ; conversion of arable land to grassland) and test if field size of each field
  ; is large enough compared to minimum required plot size
  foreach [ 0 1 2 3 ]
  [
    i -> 
    let t-possible-fields item i possible-field-agentset
    let t-area-min item i p-g-area-min-list
    let t-suitable-fields t-possible-fields with [ p-area >= t-area-min ]
    set r-suitable-fields-list lput t-suitable-fields r-suitable-fields-list
  ]
  
  report r-suitable-fields-list
end

;-----------------------------------------------------------------------------------;
; select fields that are suitable for AES the farmer accepts and is open to
; used in deliberation.nls where acceptance is derived
;-----------------------------------------------------------------------------------;
to-report r-suitable-fields-accepted [ possible-field-agentset decision-list ]
  let r-suitable-fields-list []
  
  ; loop through all AES (buffer strips, catch crops, maintaining grassland, 
  ; conversion of arable land to grassland) and test if farmer is open and accepts
  ; the AES (combined information stored in decision-list)
  foreach [ 0 1 2 3 ]
  [
    i -> 
    let t-possible-fields item i possible-field-agentset
    let t-suitable-fields ifelse-value (item i decision-list = 1)
    [ t-possible-fields ] [ no-turtles ]
    set r-suitable-fields-list lput t-suitable-fields r-suitable-fields-list
  ]
  report r-suitable-fields-list
end

;-----------------------------------------------------------------------------------;
; farmers compare contract area and envisioned area
; used in deliberation.nls where contract area is derived
;-----------------------------------------------------------------------------------;
to-report r-compare-realized-contracts [ possible-field-agentset ]
  let r-suitable-fields-list []
  
  ; loop through all AES (buffer strips, catch crops, maintaining grassland, 
  ; conversion of arable land to grassland) and test if contract area is larger or
  ; equal than area on which farmer is willing to apply AES
  foreach [ 0 1 2 3 ]
  [
    i -> 
    let t-possible-fields item i possible-field-agentset
    let t-contract-area item i v-contract-area-list
    let t-envisioned-area item i p-envisioned-area-list
    let t-contract-reached ifelse-value t-contract-area >= t-envisioned-area [ 1 ] [ 0 ]
    let t-suitable-fields ifelse-value (t-contract-reached = 1)
    [ no-turtles ] [ t-possible-fields ] 
    set r-suitable-fields-list lput t-suitable-fields r-suitable-fields-list
  ]
  report r-suitable-fields-list
end
