####################################################################################################
# RUN A MODEL ANALYSIS LOCALLY
####################################################################################################

####################################################################################################
# load libraries
####################################################################################################

# load required libraries
library(tidyverse)
library(nlrx)

# set seed
set.seed(1)

################################
# Helper function
################################

# Function to convert character vectors to actual vectors
convert_char_to_vector <- function(x) {
  if (length(x) > 1) {
    # if length > 1 (i.e. if x is the whole column of character entries), 
    # apply the function to every element of x
    return(lapply(x, convert_char_to_vector))
  } else {
    # single element: evaluate the character and return 
    return(eval(parse(text = x)))
  }
}

###################################################################################################
# Experiment setup
###################################################################################################

# Define landscape (DE-sample, CZ-sample, DE-2019, CZ-2019)
landscape <- "CZ-sample"

# Load scenarios
source("nlrx/nlrx_scenarios.R")

# Select scenario
scenario <- status_quo

# Define number of repetitions
repetitions = 2

###################################################################################################
# NLRX setup
###################################################################################################

# Path settings
netlogopath <- "C:/Program Files/NetLogo 6.2.2"
modelpath <- "code/bestmap_abm.nlogo"
outpath <- "data/output/"

# Create NL object
nl <- nl(nlversion = "6.2.2",
         nlpath = netlogopath,
         modelpath = modelpath,
         jvmmem = 1024)

# Simple simulation design using constants only
nl@experiment <- experiment(expname=scenario$expname,
                            outpath=outpath,
                            repetition=1,
                            tickmetrics="false",
                            idsetup="setup",
                            idgo="go",
                            idfinal=NA_character_,
                            idrunnum="nlrx_id",
                            runtime=NA_integer_,
                            evalticks=NA_integer_,
                            # list of all variables that are varied (empty for simdesign_simple)
                            variables = list(),
                            # list of all variables that are kept constant
                            constants = list('i-g-cluster?' = "false",
                                             'i-g-nlrx?' = 'true',
                                             'landscape' = paste0('"', landscape, '"'),
                                             'i-g-openness-calculation' = "\"distributed\"",
                                             'i-g-access-to-advisory' = scenario$advisory,
                                             'i-g-social-network-type' = "\"none\"",
                                             #'i-g-social-network-radius' = 5,
                                             #'i-g-prob-open-social' = 0.1,
                                             'i-g-prob-open-experience' = 1,
                                             'i-g-duration-buffer-strips' = scenario$duration_BS,
                                             'i-g-duration-catch-crops' = scenario$duration_CC,
                                             'i-g-duration-grassland' = scenario$duration_MG,
                                             'i-g-duration-conversion' = scenario$duration_CNV,
                                             'i-g-admin-buffer-strips' = scenario$admin_BS,
                                             'i-g-admin-catch-crops' = scenario$admin_CC,
                                             'i-g-admin-grassland' = scenario$admin_MG,
                                             'i-g-admin-conversion' = scenario$admin_CNV,
                                             'i-g-payment-buffer-strips' = scenario$payment_BS,
                                             'i-g-payment-catch-crops' = scenario$payment_CC,
                                             'i-g-payment-grassland' = scenario$payment_MG,
                                             'i-g-payment-conversion' = scenario$payment_CNV,
                                             'i-g-area-min' = 0
                            ))

# Attach simdesign simple using only constants
nl@simdesign <- simdesign_simple(nl=nl, nseeds=repetitions)

###################################################################################################
# run NLRX simulations
###################################################################################################

results <- run_nl_all(nl = nl, split = 1)

# Compile manual NetLogo output (csv) which avoids nested lists
files_list <- list.files(file.path(outpath, "temp"), pattern = scenario$expname, full.names = TRUE)

results.csv <- purrr::map_dfr(files_list, function(x) {
  x.split <- strsplit(x, "_")[[1]]
  x.tick <- as.numeric(strsplit(x.split[[length(x.split)]], "\\.")[[1]][[1]])
  x.siminputrow <- as.numeric(x.split[[length(x.split) - 1]][[1]])
  x.seed <- as.numeric(x.split[[length(x.split) - 2]][[1]])
  
  x.metrics <- read.csv(x, header = FALSE)
  colnames(x.metrics) <- c("p-field-id", "p-owner-id", "v-aes-list")
  x.metrics$`v-aes-list` <- convert_char_to_vector(x.metrics$`v-aes-list`)
  
  x.final <- tibble::tibble(siminputrow = x.siminputrow,
                            `[step]` = x.tick,
                            `random-seed` = x.seed, 
                            `field-id` = x.metrics$`p-field-id`, 
                            `owner-id` = x.metrics$`p-owner-id`, 
                            `AES_BS` = sapply(x.metrics$`v-aes-list`,"[[",1),
                            `AES_CC` = sapply(x.metrics$`v-aes-list`,"[[",2),
                            `AES_MG` = sapply(x.metrics$`v-aes-list`,"[[",3),
                            `AES_CNV` = sapply(x.metrics$`v-aes-list`,"[[",4))
  
  return(x.final)
})

# Combine NLRX results with manual csv output
results <- results %>% left_join(results.csv, by = c("siminputrow", "random-seed", "[step]"))

###################################################################################################
# save results to disk
###################################################################################################

# get current date
today <- substr(stringi::stri_replace_all_regex(as.character(Sys.Date()), "-", ""), 3, 9)

# Save results as csv files (to be combined with IACS/LPIS data)
write_csv(results, file.path(outpath, paste0(today, "_", scenario$expname, "_", landscape, "_", repetitions, ".csv")))

# Empty temp directory
temppath <- file.path(outpath, "temp")
# Get all files in the directories, recursively
f <- list.files(temppath, include.dirs = F, full.names = T, recursive = T)
# Remove the files
invisible(file.remove(f))

####################################################################################################
