#!/bin/bash
################################################################################
# CLUSTER SUBMIT SCRIPT FOR R SCRIPTS ##########################################
################################################################################

################################################################################
## SLURM ARGUMENTS
################################################################################

#### JOB NAME ##############################################
#SBATCH --job-name=status_quo
############################################################

#### RESOURCE REQUIREMENTS #################################
## runtime: <hours>:<minutes>:<seconds>
#SBATCH --time=01:00:00
## memory:
#SBATCH --mem-per-cpu=2G
## CPUs:
#SBATCH --cpus-per-task=32
############################################################

#### MAIL NOTIFICATIONS ####################################
#SBATCH --mail-user=meike.will@ufz.de
#SBATCH --mail-type=ALL,TIME_LIMIT_90
############################################################

#### WORKING DIRECTORY #####################################
#SBATCH --chdir=/home/willme/abm_bestmap
############################################################

#### OUTPUT FILES (FOR JOB LOG FILES) ######################
## %u: user name
## %x: job name
## %j: job id (single task job)
## %A: job array's master job allocation number.
##     --> Use when submitting array jobs instead of %j
## %a: Job array ID (index) number.
#SBATCH --output=/work/%u/%x-%j.log
############################################################

#### TO SUBMIT THE JOB AS AN ARRAY JOB RUN: ################
## for job testing use testing queue (remove for real runs)
## SBATCH -p testing
## sbatch submit_nlrx_simulation.sh
############################################################
################################################################################

################################################################################
## PROGRAM CALL
################################################################################
# load necessary modules
module load foss/2019b
module load R
module load Java
# if we're not running this on the cluster,
# set JOB_NAME and JOB_ID to a dummy value
JOB_NAME=${SLURM_JOB_NAME:-dummy}
JOB_ID=${SLURM_JOB_ID:-0}
# if number of workers has not been specified, use 1 as
# fallback value (i.e. sequential execution)
N_WORKERS=${SLURM_CPUS_PER_TASK:-1}

# SPECIFY R SCRIPT TO RUN ##################################
RUN_SCRIPT="nlrx/run_nlrx_simulation.R"
############################################################

# SET DIRECTORY NAMES ######################################
# WORK_DIR = location of cluster run scripts
WORK_DIR="/home/$USER/abm_bestmap/"
# MODEL_DIR = path to NetLogo model file
MODEL_DIR="/home/$USER/abm_bestmap/code/bestmap_abm.nlogo"
# OUTPUT_DIR = directory where all model results are saved
OUTPUT_DIR="/work/$USER/$JOB_NAME-$JOB_ID/"
# NETLOGO_DIR = path to NetLogo executable
NETLOGO_DIR="/data/polises/Software/NetLogo_622/"
# NETLOGO_VERSION_NUMBER
NETLOGO_VERSION_NUMBER="6.2.2"
############################################################

# create the output directory
mkdir -p "/work/$USER/$JOB_NAME-$JOB_ID"

# RUN THE MODEL ############################################
"$WORK_DIR/$RUN_SCRIPT" \
    --job_name="$JOB_NAME" \
	--job_id="$JOB_ID" \
	--n_workers="$N_WORKERS" \
	--model="$MODEL_DIR" \
	--output="$OUTPUT_DIR" \
	--netlogo="$NETLOGO_DIR" \
	--netlogo_version="$NETLOGO_VERSION_NUMBER"
############################################################

# END ##########################################################################
