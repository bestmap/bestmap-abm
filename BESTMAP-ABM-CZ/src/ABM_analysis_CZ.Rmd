---
title: "Compare ABM output with LPIS data"
author: "Meike Will"
date: "Last compiled on `r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document:
    code_folding: hide
    toc: yes
    number_sections: true
    toc_float: TRUE
---

```{r, message=FALSE, results=FALSE}
library(sf)
library(here)
library(tidyverse)

#####################################
## Fields
#####################################

# load field ABM data (2019)
fields = st_read(here("data", "landscape", "CZ-2019", "field-file.shp"))

# add presence/absence for selected AES
fields <- fields %>%
  mutate(AES_selected = ifelse(rowSums(across(c(AES_BS, AES_CC, AES_MG, AES_CNV))) > 0, 1, 0))

# add AES area
fields <- fields %>%
  mutate(
    # cover crops: not available as AES in CZ
    AES_CC_area = 0, 
    # maintaining grassland: 10.1.4(1-10)
    AES_MG_area = AES_MG * AREA_HA, 
    # buffer areas: 10.1.6(1-2)
    AES_BS_area = AES_BS * AREA_HA, 
    # conversion to grassland: 10.1.5(1-6)
    AES_CNV_area = AES_CNV * AREA_HA, 
    # area under selected AES
    AES_area = AREA_HA * AES_selected)

#####################################
## Farmers
#####################################

# load farmer ABM data (2019)
farmers = read.csv(here("data", "landscape", "CZ-2019", "farmers.csv"), header = TRUE)

# summarize field data at farm level (presence/absence)
farms <- fields %>% 
  st_drop_geometry() %>%
  dplyr::select(-c(FIELDID, LANDUSE, EFA)) %>%
  group_by(OWNERID) %>% 
  mutate(numberFields = n()) %>%  # number of fields
  group_by(OWNERID, numberFields) %>%
  summarise_all(sum)

# replace count with 1 to denote presence/absence
# AES count
AES_list <- c("AES_BS", "AES_CC", "AES_MG", "AES_CNV", "AES_selected")
farms[,AES_list][farms[,AES_list] > 0] <- 1

# add FSA information to aggregated farm data
farms <- left_join(farms, farmers)

# select relevant columns
fields <- fields %>% dplyr::select(FIELDID, OWNERID, AREA_HA, LANDUSE, EFA, geometry, AES_selected, AES_area,
                                   AES_BS, AES_CC, AES_MG, AES_CNV)

# ungroup farms and fields data.frame to avoid errors
farms <- farms %>% ungroup()
fields <- fields %>% ungroup()

#####################################
## ABM output - fields
#####################################

# Select simulations
id_sq <- "30896391"
# id_sc <- "30729317"
# id_ad <- "30729298"
# id_hp <- "30729297"
# id_la <- "30729439"

landscape <- "CZ-2019"
repetitions <- "50"

# read ABM output: status quo (sq)
path <- here("data", "output", paste0(id_sq, "_status_quo_", landscape, "_", repetitions, ".csv"))
status_quo = read.csv(path)
status_quo <- status_quo %>% 
  dplyr::select(random.seed, field.id, owner.id, AES_BS, AES_CC, AES_MG, AES_CNV) %>%
  rename(FIELDID = field.id,
         OWNERID = owner.id,
         AES_BS_ABM_sq = AES_BS,
         AES_CC_ABM_sq = AES_CC,
         AES_MG_ABM_sq = AES_MG,
         AES_CNV_ABM_sq = AES_CNV) %>%
  mutate(AES_ABM_sq = ifelse(rowSums(across(c(AES_BS_ABM_sq, AES_CC_ABM_sq, AES_MG_ABM_sq, AES_CNV_ABM_sq))) > 0, 1, 0))

# # read ABM output: short contracts (sc)
# path <- here("data", "output", paste0(id_sc, "_short_contracts_", landscape, "_", repetitions, ".csv"))
# short_contracts = read.csv(path)
# short_contracts <- short_contracts %>%
#   dplyr::select(random.seed, field.id, owner.id, AES_BS, AES_CC, AES_MG, AES_CNV) %>%
#   rename(FIELDID = field.id,
#          OWNERID = owner.id,
#          AES_BS_ABM_sc = AES_BS,
#          AES_CC_ABM_sc = AES_CC,
#          AES_MG_ABM_sc = AES_MG,
#          AES_CNV_ABM_sc = AES_CNV) %>%
#   mutate(AES_ABM_sc = ifelse(rowSums(across(c(AES_BS_ABM_sc, AES_CC_ABM_sc, AES_MG_ABM_sc, AES_CNV_ABM_sc))) > 0, 1, 0))
# 
# # read ABM output: high payment (hp)
# path <- here("data", "output", paste0(id_hp, "_high_payment_", landscape, "_", repetitions, ".csv"))
# high_payment = read.csv(path)
# high_payment <- high_payment %>%
#   dplyr::select(random.seed, field.id, owner.id, AES_BS, AES_CC, AES_MG, AES_CNV) %>%
#   rename(FIELDID = field.id,
#          OWNERID = owner.id,
#          AES_BS_ABM_hp = AES_BS,
#          AES_CC_ABM_hp = AES_CC,
#          AES_MG_ABM_hp = AES_MG,
#          AES_CNV_ABM_hp = AES_CNV) %>%
#   mutate(AES_ABM_hp = ifelse(rowSums(across(c(AES_BS_ABM_hp, AES_CC_ABM_hp, AES_MG_ABM_hp, AES_CNV_ABM_hp))) > 0, 1, 0))
# 
# # read ABM output: advisory support (ad)
# path <- here("data", "output", paste0(id_ad, "_advisory_", landscape, "_", repetitions, ".csv"))
# advisory = read.csv(path)
# advisory <- advisory %>%
#   dplyr::select(random.seed, field.id, owner.id, AES_BS, AES_CC, AES_MG, AES_CNV) %>%
#   rename(FIELDID = field.id,
#          OWNERID = owner.id,
#          AES_BS_ABM_ad = AES_BS,
#          AES_CC_ABM_ad = AES_CC,
#          AES_MG_ABM_ad = AES_MG,
#          AES_CNV_ABM_ad = AES_CNV) %>%
#   mutate(AES_ABM_ad = ifelse(rowSums(across(c(AES_BS_ABM_ad, AES_CC_ABM_ad, AES_MG_ABM_ad, AES_CNV_ABM_ad))) > 0, 1, 0))
# 
# # read ABM output: low administrative effort (la)
# path <- here("data", "output", paste0(id_la, "_low_admin_", landscape, "_", repetitions, ".csv"))
# low_admin = read.csv(path)
# low_admin <- low_admin %>%
#   dplyr::select(random.seed, field.id, owner.id, AES_BS, AES_CC, AES_MG, AES_CNV) %>%
#   rename(FIELDID = field.id,
#          OWNERID = owner.id,
#          AES_BS_ABM_la = AES_BS,
#          AES_CC_ABM_la = AES_CC,
#          AES_MG_ABM_la = AES_MG,
#          AES_CNV_ABM_la = AES_CNV) %>%
#   mutate(AES_ABM_la = ifelse(rowSums(across(c(AES_BS_ABM_la, AES_CC_ABM_la, AES_MG_ABM_la, AES_CNV_ABM_la))) > 0, 1, 0))

# probability for adoption at field level: summarize over multiple runs
status_quo_fields <- status_quo %>%
  dplyr::select(-random.seed) %>%
  group_by(FIELDID, OWNERID) %>%
  summarize_all(mean) %>% 
  ungroup()

# short_contracts_fields <- short_contracts %>%
#   dplyr::select(-random.seed) %>%
#   group_by(FIELDID, OWNERID) %>%
#   summarize_all(mean) %>%
#   ungroup()
# 
# high_payment_fields <- high_payment %>%
#   dplyr::select(-random.seed) %>%
#   group_by(FIELDID, OWNERID) %>%
#   summarize_all(mean) %>%
#   ungroup()
# 
# advisory_fields <- advisory %>%
#   dplyr::select(-random.seed) %>%
#   group_by(FIELDID, OWNERID) %>%
#   summarize_all(mean) %>%
#   ungroup()
# 
# low_admin_fields <- low_admin %>%
#   dplyr::select(-random.seed) %>%
#   group_by(FIELDID, OWNERID) %>%
#   summarize_all(mean) %>%
#   ungroup()

# combine scenario outputs at field level
fields_ABM <- status_quo_fields
# fields_ABM <- left_join(status_quo_fields, short_contracts_fields)
# fields_ABM <- left_join(fields_ABM, high_payment_fields)
# fields_ABM <- left_join(fields_ABM, advisory_fields)
# fields_ABM <- left_join(fields_ABM, low_admin_fields)

# sample subset (if ABM is only simulated on sampled fields)
if(grepl("sample", path, fixed = TRUE)){
  sample <- unique(fields_ABM$OWNERID)
  fields <- fields %>% dplyr::filter(OWNERID %in% sample) 
}

# combine LPIS data and ABM output at field level
fields <- right_join(fields, fields_ABM)

# calculate AES area for ABM-AES
fields <- fields %>% 
  mutate(ABM_area_sq = AES_ABM_sq * AREA_HA, 
         ABM_area_BS_sq = AES_BS_ABM_sq * AREA_HA,
         ABM_area_CC_sq = AES_CC_ABM_sq * AREA_HA,
         ABM_area_MG_sq = AES_MG_ABM_sq * AREA_HA,
         ABM_area_CNV_sq = AES_CNV_ABM_sq * AREA_HA)

#####################################
## ABM output - farms
#####################################

# select relevant columns
farms <- farms %>% dplyr::select(OWNERID, AREA_HA, AES_BS, AES_CC, AES_MG, AES_CNV, FSA)

# presence/absence for selected AES
farms <- farms %>%
  mutate(AES_selected = ifelse(rowSums(across(c(AES_BS, AES_CC, AES_MG, AES_CNV))) > 0, 1, 0))

# sample subset (if ABM is only simulated on sampled fields)
if(grepl("sample", path, fixed = TRUE)){
  farms <- farms %>% dplyr::filter(OWNERID %in% sample)
  rm(sample)
}

# summarize ABM field data at farm level (presence/absence) for individual runs
status_quo_farms <- status_quo %>%
  dplyr::select(-c("FIELDID")) %>%
  group_by(OWNERID, random.seed) %>%
  summarise_all(sum)

# short_contracts_farms <- short_contracts %>%
#   dplyr::select(-c("FIELDID")) %>%
#   group_by(OWNERID, random.seed) %>%
#   summarise_all(sum)
# 
# high_payment_farms <- high_payment %>%
#   dplyr::select(-c("FIELDID")) %>%
#   group_by(OWNERID, random.seed) %>%
#   summarise_all(sum)
# 
# advisory_farms <- advisory %>%
#   dplyr::select(-c("FIELDID")) %>%
#   group_by(OWNERID, random.seed) %>%
#   summarise_all(sum)
# 
# low_admin_farms <- low_admin %>%
#   dplyr::select(-c("FIELDID")) %>%
#   group_by(OWNERID, random.seed) %>%
#   summarise_all(sum)

# combine scenario outputs at farm level
farms_ABM <- status_quo_farms
# farms_ABM <- left_join(status_quo_farms, short_contracts_farms)
# farms_ABM <- left_join(farms_ABM, high_payment_farms)
# farms_ABM <- left_join(farms_ABM, advisory_farms)
# farms_ABM <- left_join(farms_ABM, low_admin_farms)

# replace count with 1 to denote presence/absence (except for OWNERID and random.seed)
farms_ABM[,-which(names(farms_ABM) %in% c("OWNERID", "random.seed"))][farms_ABM[,-which(names(farms_ABM) %in% c("OWNERID", "random.seed"))] > 0] <- 1

# mean farm participation: summarize farm participation over multiple runs
farms_ABM <- farms_ABM %>%
  group_by(OWNERID) %>%
  dplyr::select(-c(random.seed)) %>%
  summarise_all(mean)

# combine LPIS data and ABM output at farm level
farms <- left_join(farms_ABM, farms)

# split FSA by specialization and economic size
farms <- farms %>% 
  mutate(FarmSpec = sub("\\_.*", "", FSA),
         EconSize = sub(".*\\_", "", FSA))

# remove temp data
rm(advisory, advisory_farms, advisory_fields, farmers, farms_ABM, fields_ABM, AES_list, 
   high_payment, high_payment_farms, high_payment_fields, low_admin, low_admin_farms, low_admin_fields,
   short_contracts, short_contracts_farms, short_contracts_fields, status_quo, status_quo_farms, status_quo_fields,
   id_sq, id_sc, id_ad, id_hp, id_la, landscape, path, repetitions)
```

# Analysis at farm level
## Status quo
### AES adoption: all selected AES
*Which fraction of farms apply at least one of the selected AES?*
-   Cover crops (not offered in CZ)
-   Buffer areas (10.1.6(1-2))
-   Conversion of arable land to permanent grassland (10.1.5(1-6))
-   Maintaining grassland (10.1.4(1-10))

```{r}
farms %>%
  dplyr::select(AES_selected, AES_ABM_sq) %>%
  pivot_longer(cols = c(AES_selected, AES_ABM_sq), 
               names_to = "AES_group", values_to = "values") %>%
  mutate(AES_group = factor(AES_group, levels = levels(factor(AES_group))[c(2,1)])) %>%
  group_by(AES_group) %>%
  summarize(fraction = sum(values)/n()) %>%
  ggplot(., aes(x= AES_group, y = fraction, fill = AES_group)) +
  geom_bar(stat = "identity", color = "black") +
  # geom_text(aes(label = paste0(round(fraction*100,2), "%")), vjust = -.3) #+
  scale_fill_manual(values=c("AES_selected" = "#bfbfbf", "AES_ABM_sq" = "#737373")) +
  scale_x_discrete(labels = c("LPIS","ABM (status quo)")) +
  scale_y_continuous(labels=scales::percent) +
  labs(title = "Fraction of farms applying selected AES", x="", y = "Fraction") +
  theme_bw() +
  theme(legend.position = "none")
```

```{r}
fields %>%
  st_drop_geometry() %>%
  dplyr::select(AES_area, ABM_area_sq) %>%
  pivot_longer(cols = c(AES_area, ABM_area_sq), 
               names_to = "AES_group", values_to = "values") %>%
  mutate(AES_group = factor(AES_group, levels = levels(factor(AES_group))[c(2,1)])) %>%
  group_by(AES_group) %>%
  summarize(fraction = sum(values)) %>% #/n()) %>%
  ggplot(., aes(x= AES_group, y = fraction, fill = AES_group)) +
  geom_bar(stat = "identity", color = "black") +
  geom_text(aes(label = fraction)) +#paste0(round(fraction*100,2), "%")), vjust = -.3) +
  scale_fill_manual(values=c("AES_area" = "#bfbfbf", "ABM_area_sq" = "#737373")) +
  scale_x_discrete(labels = c("LPIS","ABM (status quo)")) +
  # scale_y_continuous(labels=scales::percent) +
  labs(title = "Area under selected AES", x="", y = "Fraction") +
  theme_bw() +
  theme(legend.position = "none")
```

### AES adoption: AES groups 
*Which fraction of farms apply at least one AES of the grouped AES, respectively?*
-   Cover crops (not offered in CZ)
-   Buffer areas (10.1.6(1-2))
-   Conversion of arable land to permanent grassland (10.1.5(1-6))
-   Maintaining grassland (10.1.4(1-10))

```{r}
farms %>%
  mutate(total_nr = n()) %>%
  pivot_longer(cols = c(AES_CC, AES_BS, AES_MG, AES_CNV, 
                        AES_CC_ABM_sq, AES_BS_ABM_sq, AES_MG_ABM_sq, AES_CNV_ABM_sq),
               names_to = "AES_group", values_to = "values") %>%
  mutate(type = ifelse(grepl("ABM", AES_group, fixed = TRUE), "ABM", "LPIS"),
         type = factor(type, levels = levels(factor(type))[c(2,1)]), 
         AES_group = ifelse(grepl("ABM", AES_group, fixed = TRUE),
                            str_replace(AES_group, "_ABM_sq", ""), AES_group)) %>%
  mutate(AES_group = factor(AES_group, levels = levels(factor(AES_group))[c(2,1,3,4)])) %>%
  mutate(AES_group = factor(AES_group, labels = c("cover crops", "buffer areas", "conversion to grassland",
                                                  "maintaining grassland"))) %>%
  group_by(AES_group, type) %>%
  summarize(fraction = sum(values)/total_nr, .groups = "keep") %>%
  slice(1) %>%
  # ungroup() %>%
  # complete(AES_group, fill = list(fraction = 0)) %>% 
  ggplot(., aes(x=as.factor(AES_group), y = fraction, fill = type)) +
  geom_bar(stat="identity", color = "black", position = position_dodge()) +
  geom_text(aes(label = paste0(round(fraction*100,2), "%")),
            position = position_dodge(width=1), vjust = -.3, size = 3) +
  scale_fill_manual(values=c("LPIS" = "#bfbfbf", "ABM" = "#737373")) +
  scale_x_discrete(drop = FALSE, labels = c("conversion to grassland" = "conversion to\ngrassland",
                                            "maintaining grassland" = "maintaining\ngrassland")) +
  scale_y_continuous(labels = scales::percent_format()) +
  labs(title = "Fraction of farms applying grouped AES (status quo)", x="", y = "Fraction", fill = "") +
  theme_bw()
```

### Spatial AES distribution
```{r}
fields %>%
  ggplot() +
  geom_sf(aes(fill = AES_ABM_sq), color = NA) +
  scale_fill_gradient(low = "#F7FCF5", high = "#006D2C", limits = c(0,1)) +
  labs(fill = "Probability for AES") +
  guides(fill = guide_colorbar(frame.colour=c("black"), frame.linewidth=1, ticks.colour="black", title.position="top")) +
  theme_void() +
  theme(legend.position = "bottom", legend.box = "horizontal")
```

## Short contracts (1 year)
### AES adoption: all selected AES
*Which fraction of farms apply at least one of the selected AES?*
-   Cover crops (not offered in CZ)
-   Buffer areas (10.1.6(1-2))
-   Conversion of arable land to permanent grassland (10.1.5(1-6))
-   Maintaining grassland (10.1.4(1-10))

```{r}
farms %>%
  dplyr::select(AES_selected, AES_ABM_sc) %>%
  pivot_longer(cols = c(AES_selected, AES_ABM_sc), 
               names_to = "AES_group", values_to = "values") %>%
  mutate(AES_group = factor(AES_group, levels = levels(factor(AES_group))[c(2,1)])) %>%
  group_by(AES_group) %>%
  summarize(fraction = sum(values)/n()) %>%
  ggplot(., aes(x= AES_group, y = fraction, fill = AES_group)) +
  geom_bar(stat = "identity", color = "black") +
  # geom_text(aes(label = paste0(round(fraction*100,2), "%")), vjust = -.3) +
  scale_fill_manual(values=c("AES_selected" = "#bfbfbf", "AES_ABM_sc" = "#737373")) +
  scale_x_discrete(labels = c("LPIS","ABM (short contracts)")) +
  scale_y_continuous(labels=scales::percent) +
  labs(title = "Fraction of farms applying selected AES", x="", y = "Fraction") +
  theme_bw() +
  theme(legend.position = "none")
```

### AES adoption: AES groups 
*Which fraction of farms apply at least one AES of the grouped AES, respectively?*
-   Cover crops (not offered in CZ)
-   Buffer areas (10.1.6(1-2))
-   Conversion of arable land to permanent grassland (10.1.5(1-6))
-   Maintaining grassland (10.1.4(1-10))

```{r}
farms %>%
  mutate(total_nr = n()) %>%
  pivot_longer(cols = c(AES_CC, AES_BS, AES_MG, AES_CNV, 
                        AES_CC_ABM_sc, AES_BS_ABM_sc, AES_MG_ABM_sc, AES_CNV_ABM_sc),
               names_to = "AES_group", values_to = "values") %>%
  mutate(type = ifelse(grepl("ABM", AES_group, fixed = TRUE), "ABM", "LPIS"),
         type = factor(type, levels = levels(factor(type))[c(2,1)]), 
         AES_group = ifelse(grepl("ABM", AES_group, fixed = TRUE),
                            str_replace(AES_group, "_ABM_sc", ""), AES_group)) %>%
  mutate(AES_group = factor(AES_group, levels = levels(factor(AES_group))[c(2,1,3,4)])) %>%
  mutate(AES_group = factor(AES_group, labels = c("cover crops", "buffer areas", "conversion to grassland",
                                                  "maintaining grassland"))) %>%
  group_by(AES_group, type) %>%
  summarize(fraction = sum(values)/total_nr, .groups = "keep") %>%
  slice(1) %>%
  # ungroup() %>%
  # complete(AES_group, fill = list(fraction = 0)) %>% 
  ggplot(., aes(x=as.factor(AES_group), y = fraction, fill = type)) +
  geom_bar(stat="identity", color = "black", position = position_dodge()) +
  # geom_text(aes(label = paste0(round(fraction*100,2), "%")),
  #           position = position_dodge(width=1), vjust = -.3, size = 3) +
  scale_fill_manual(values=c("LPIS" = "#bfbfbf", "ABM" = "#737373")) +
  scale_x_discrete(drop = FALSE, labels = c("conversion to grassland" = "conversion to\ngrassland",
                                            "maintaining grassland" = "maintaining\ngrassland")) +
  scale_y_continuous(labels = scales::percent_format()) +
  labs(title = "Fraction of farms applying grouped AES (short contracts)", x="", y = "Fraction", fill = "") +
  theme_bw()
```

### Spatial AES distribution
```{r}
fields %>%
  ggplot() +
  geom_sf(aes(fill = AES_ABM_sc), color = NA) +
  scale_fill_gradient(low = "#F7FCF5", high = "#006D2C", limits = c(0,1)) +
  labs(fill = "Probability for AES") +
  guides(fill = guide_colorbar(frame.colour=c("black"), frame.linewidth=1, ticks.colour="black", title.position="top")) +
  theme_void() +
  theme(legend.position = "bottom", legend.box = "horizontal")
```

## High payments (+ 10%)
### AES adoption: all selected AES
*Which fraction of farms apply at least one of the selected AES?*
-   Cover crops (not offered in CZ)
-   Buffer areas (10.1.6(1-2))
-   Conversion of arable land to permanent grassland (10.1.5(1-6))
-   Maintaining grassland (10.1.4(1-10))

```{r}
farms %>%
  dplyr::select(AES_selected, AES_ABM_hp) %>%
  pivot_longer(cols = c(AES_selected, AES_ABM_hp), 
               names_to = "AES_group", values_to = "values") %>%
  mutate(AES_group = factor(AES_group, levels = levels(factor(AES_group))[c(2,1)])) %>%
  group_by(AES_group) %>%
  summarize(fraction = sum(values)/n()) %>%
  ggplot(., aes(x= AES_group, y = fraction, fill = AES_group)) +
  geom_bar(stat = "identity", color = "black") +
  # geom_text(aes(label = paste0(round(fraction*100,2), "%")), vjust = -.3) +
  scale_fill_manual(values=c("AES_selected" = "#bfbfbf", "AES_ABM_hp" = "#737373")) +
  scale_x_discrete(labels = c("LPIS","ABM (high payments)")) +
  scale_y_continuous(labels=scales::percent) +
  labs(title = "Fraction of farms applying selected AES", x="", y = "Fraction") +
  theme_bw() +
  theme(legend.position = "none")
```

### AES adoption: AES groups 
*Which fraction of farms apply at least one AES of the grouped AES, respectively?*
-   Cover crops (not offered in CZ)
-   Buffer areas (10.1.6(1-2))
-   Conversion of arable land to permanent grassland (10.1.5(1-6))
-   Maintaining grassland (10.1.4(1-10))

```{r}
farms %>%
  mutate(total_nr = n()) %>%
  pivot_longer(cols = c(AES_CC, AES_BS, AES_MG, AES_CNV, 
                        AES_CC_ABM_hp, AES_BS_ABM_hp, AES_MG_ABM_hp, AES_CNV_ABM_hp),
               names_to = "AES_group", values_to = "values") %>%
  mutate(type = ifelse(grepl("ABM", AES_group, fixed = TRUE), "ABM", "LPIS"),
         type = factor(type, levels = levels(factor(type))[c(2,1)]), 
         AES_group = ifelse(grepl("ABM", AES_group, fixed = TRUE),
                            str_replace(AES_group, "_ABM_hp", ""), AES_group)) %>%
  mutate(AES_group = factor(AES_group, levels = levels(factor(AES_group))[c(2,1,3,4)])) %>%
  mutate(AES_group = factor(AES_group, labels = c("cover crops", "buffer areas", "conversion to grassland",
                                                  "maintaining grassland"))) %>%
  group_by(AES_group, type) %>%
  summarize(fraction = sum(values)/total_nr, .groups = "keep") %>%
  slice(1) %>%
  # ungroup() %>%
  # complete(AES_group, fill = list(fraction = 0)) %>% 
  ggplot(., aes(x=as.factor(AES_group), y = fraction, fill = type)) +
  geom_bar(stat="identity", color = "black", position = position_dodge()) +
  # geom_text(aes(label = paste0(round(fraction*100,2), "%")),
  #           position = position_dodge(width=1), vjust = -.3, size = 3) +
  scale_fill_manual(values=c("LPIS" = "#bfbfbf", "ABM" = "#737373")) +
  scale_x_discrete(drop = FALSE, labels = c("conversion to grassland" = "conversion to\ngrassland",
                                            "maintaining grassland" = "maintaining\ngrassland")) +
  scale_y_continuous(labels = scales::percent_format()) +
  labs(title = "Fraction of farms applying grouped AES (high payments)", x="", y = "Fraction", fill = "") +
  theme_bw()
```

### Spatial AES distribution
```{r}
fields %>%
  ggplot() +
  geom_sf(aes(fill = AES_ABM_hp), color = NA) +
  scale_fill_gradient(low = "#F7FCF5", high = "#006D2C", limits = c(0,1)) +
  labs(fill = "Probability for AES") +
  guides(fill = guide_colorbar(frame.colour=c("black"), frame.linewidth=1, ticks.colour="black", title.position="top")) +
  theme_void() +
  theme(legend.position = "bottom", legend.box = "horizontal")
```

## Advisory support
### AES adoption: all selected AES
*Which fraction of farms apply at least one of the selected AES?*
-   Cover crops (not offered in CZ)
-   Buffer areas (10.1.6(1-2))
-   Conversion of arable land to permanent grassland (10.1.5(1-6))
-   Maintaining grassland (10.1.4(1-10))

```{r}
farms %>%
  dplyr::select(AES_selected, AES_ABM_ad) %>%
  pivot_longer(cols = c(AES_selected, AES_ABM_ad), 
               names_to = "AES_group", values_to = "values") %>%
  mutate(AES_group = factor(AES_group, levels = levels(factor(AES_group))[c(2,1)])) %>%
  group_by(AES_group) %>%
  summarize(fraction = sum(values)/n()) %>%
  ggplot(., aes(x= AES_group, y = fraction, fill = AES_group)) +
  geom_bar(stat = "identity", color = "black") +
  # geom_text(aes(label = paste0(round(fraction*100,2), "%")), vjust = -.3) +
  scale_fill_manual(values=c("AES_selected" = "#bfbfbf", "AES_ABM_ad" = "#737373")) +
  scale_x_discrete(labels = c("LPIS","ABM (advisory)")) +
  scale_y_continuous(labels=scales::percent) +
  labs(title = "Fraction of farms applying selected AES", x="", y = "Fraction") +
  theme_bw() +
  theme(legend.position = "none")
```

### AES adoption: AES groups 
*Which fraction of farms apply at least one AES of the grouped AES, respectively?*
-   Cover crops (not offered in CZ)
-   Buffer areas (10.1.6(1-2))
-   Conversion of arable land to permanent grassland (10.1.5(1-6))
-   Maintaining grassland (10.1.4(1-10))

```{r}
farms %>%
  mutate(total_nr = n()) %>%
  pivot_longer(cols = c(AES_CC, AES_BS, AES_MG, AES_CNV, 
                        AES_CC_ABM_ad, AES_BS_ABM_ad, AES_MG_ABM_ad, AES_CNV_ABM_ad),
               names_to = "AES_group", values_to = "values") %>%
  mutate(type = ifelse(grepl("ABM", AES_group, fixed = TRUE), "ABM", "LPIS"),
         type = factor(type, levels = levels(factor(type))[c(2,1)]), 
         AES_group = ifelse(grepl("ABM", AES_group, fixed = TRUE),
                            str_replace(AES_group, "_ABM_ad", ""), AES_group)) %>%
  mutate(AES_group = factor(AES_group, levels = levels(factor(AES_group))[c(2,1,3,4)])) %>%
  mutate(AES_group = factor(AES_group, labels = c("cover crops", "buffer areas", "conversion to grassland",
                                                  "maintaining grassland"))) %>%
  group_by(AES_group, type) %>%
  summarize(fraction = sum(values)/total_nr, .groups = "keep") %>%
  slice(1) %>%
  # ungroup() %>%
  # complete(AES_group, fill = list(fraction = 0)) %>%
  ggplot(., aes(x=as.factor(AES_group), y = fraction, fill = type)) +
  geom_bar(stat="identity", color = "black", position = position_dodge()) +
  # geom_text(aes(label = paste0(round(fraction*100,2), "%")),
  #           position = position_dodge(width=1), vjust = -.3, size = 3) +
  scale_fill_manual(values=c("LPIS" = "#bfbfbf", "ABM" = "#737373")) +
  scale_x_discrete(drop = FALSE, labels = c("conversion to grassland" = "conversion to\ngrassland",
                                            "maintaining grassland" = "maintaining\ngrassland")) +
  scale_y_continuous(labels = scales::percent_format()) +
  labs(title = "Fraction of farms applying grouped AES (advisory)", x="", y = "Fraction", fill = "") +
  theme_bw()
```

### Spatial AES distribution
```{r}
fields %>%
  ggplot() +
  geom_sf(aes(fill = AES_ABM_ad), color = NA) +
  scale_fill_gradient(low = "#F7FCF5", high = "#006D2C", limits = c(0,1)) +
  labs(fill = "Probability for AES") +
  guides(fill = guide_colorbar(frame.colour=c("black"), frame.linewidth=1, ticks.colour="black", title.position="top")) +
  theme_void() +
  theme(legend.position = "bottom", legend.box = "horizontal")
```

# All scenarios
*Which fraction of farms apply at least one AES of the grouped AES, respectively?*
-   Cover crops (not offered in CZ)
-   Buffer areas (10.1.6(1-2))
-   Conversion of arable land to permanent grassland (10.1.5(1-6))
-   Maintaining grassland (10.1.4(1-10))

```{r}
farms %>%
  mutate(total_nr = n()) %>%
  pivot_longer(cols = c(#AES_CNV, AES_BS, AES_MG,
    AES_CNV_ABM_sq, AES_BS_ABM_sq, AES_MG_ABM_sq, AES_CC_ABM_sq, 
    AES_CNV_ABM_ad, AES_BS_ABM_ad, AES_MG_ABM_ad, AES_CC_ABM_ad,
    AES_CNV_ABM_sc, AES_BS_ABM_sc, AES_MG_ABM_sc, AES_CC_ABM_sc,
    AES_CNV_ABM_hp, AES_BS_ABM_hp, AES_MG_ABM_hp, AES_CC_ABM_hp,
    AES_CNV_ABM_la, AES_BS_ABM_la, AES_MG_ABM_la, AES_CC_ABM_la),
    names_to = "AES_group", values_to = "values") %>%
  mutate(type = ifelse(grepl("ABM_sq", AES_group, fixed = TRUE), "Status quo", #"ABM (Status quo)",
                       ifelse(grepl("ABM_ad", AES_group, fixed = TRUE), "Advisory",
                              ifelse(grepl("ABM_sc", AES_group, fixed = TRUE), "Short contracts",
                                     ifelse(grepl("ABM_hp", AES_group, fixed = TRUE), "High payment",
                                            ifelse(grepl("ABM_la", AES_group, fixed = TRUE), "Low administrative effort",
                                                   "LPIS"))))),
         type = factor(type, levels = levels(factor(type))[c(5,1,2,4,3)]),
         AES_group = ifelse(grepl("ABM_sq", AES_group, fixed = TRUE),
                            str_replace(AES_group, "_ABM_sq", ""), AES_group),
         AES_group = ifelse(grepl("ABM_ad", AES_group, fixed = TRUE),
                            str_replace(AES_group, "_ABM_ad", ""), AES_group),
         AES_group = ifelse(grepl("ABM_sc", AES_group, fixed = TRUE),
                            str_replace(AES_group, "_ABM_sc", ""), AES_group),
         AES_group = ifelse(grepl("ABM_hp", AES_group, fixed = TRUE),
                            str_replace(AES_group, "_ABM_hp", ""), AES_group), 
         AES_group = ifelse(grepl("ABM_la", AES_group, fixed = TRUE),
                            str_replace(AES_group, "_ABM_la", ""), AES_group)) %>%
  mutate(AES_group = factor(AES_group, levels = levels(factor(AES_group))[c(1,2,4,3)])) %>%
  mutate(AES_group = factor(AES_group, labels = c("buffer areas", "cover crops", 
                                                  "maintaining grassland", "conversion to grassland"))) %>%
  group_by(AES_group, type) %>%
  summarize(fraction = sum(values)/total_nr, .groups = "keep") %>%
  slice(1) %>%
  ggplot(., aes(x=as.factor(AES_group), y = fraction, fill = type)) +
  geom_bar(stat="identity", color = "black", position = position_dodge()) +
  scale_fill_manual(values=c("#ffffcc", "#a1dab4", "#41b6c4", "#2c7fb8", "#253494")) +
  scale_x_discrete(drop = FALSE, labels = c("maintaining grassland" = "maintaining\ngrassland", 
                                            "conversion to grassland" = "conversion to\ngrassland")) +
  scale_y_continuous(labels = scales::percent_format()) +
  labs(x="", y = "Fraction of farms\nwith specific AES", fill = "") +
  guides(fill=guide_legend(nrow=2, byrow = TRUE)) +
  theme_bw() +
  theme(legend.position = "bottom", legend.margin=margin(0,0,0,0),
        # text = element_text(size = 15)
  )

ggsave("all.png", width = 14, height = 8, units = "cm")
```

```{r}
farms %>%
  mutate(FarmSpec = factor(FarmSpec, levels = levels(factor(FarmSpec))[c(2,3,1)])) %>%
  group_by(FarmSpec) %>%
  mutate(total_nr = n()) %>%
  pivot_longer(cols = c(#AES_CNV, AES_BS, AES_MG,
    AES_CNV_ABM_sq, AES_BS_ABM_sq, AES_MG_ABM_sq, AES_CC_ABM_sq, 
    AES_CNV_ABM_ad, AES_BS_ABM_ad, AES_MG_ABM_ad, AES_CC_ABM_ad,
    AES_CNV_ABM_sc, AES_BS_ABM_sc, AES_MG_ABM_sc, AES_CC_ABM_sc,
    AES_CNV_ABM_hp, AES_BS_ABM_hp, AES_MG_ABM_hp, AES_CC_ABM_hp,
    AES_CNV_ABM_la, AES_BS_ABM_la, AES_MG_ABM_la, AES_CC_ABM_la),
    names_to = "AES_group", values_to = "values") %>%
  mutate(type = ifelse(grepl("ABM_sq", AES_group, fixed = TRUE), "Status quo", #"ABM (Status quo)",
                       ifelse(grepl("ABM_ad", AES_group, fixed = TRUE), "Advisory",
                              ifelse(grepl("ABM_sc", AES_group, fixed = TRUE), "Short contracts",
                                     ifelse(grepl("ABM_hp", AES_group, fixed = TRUE), "High payment",
                                            ifelse(grepl("ABM_la", AES_group, fixed = TRUE), "Low administrative effort",
                                                   "LPIS"))))),
         type = factor(type, levels = levels(factor(type))[c(5,1,2,4,3)]),
         AES_group = ifelse(grepl("ABM_sq", AES_group, fixed = TRUE),
                            str_replace(AES_group, "_ABM_sq", ""), AES_group),
         AES_group = ifelse(grepl("ABM_ad", AES_group, fixed = TRUE),
                            str_replace(AES_group, "_ABM_ad", ""), AES_group),
         AES_group = ifelse(grepl("ABM_sc", AES_group, fixed = TRUE),
                            str_replace(AES_group, "_ABM_sc", ""), AES_group),
         AES_group = ifelse(grepl("ABM_hp", AES_group, fixed = TRUE),
                            str_replace(AES_group, "_ABM_hp", ""), AES_group), 
         AES_group = ifelse(grepl("ABM_la", AES_group, fixed = TRUE),
                            str_replace(AES_group, "_ABM_la", ""), AES_group)) %>%
  mutate(AES_group = factor(AES_group, levels = levels(factor(AES_group))[c(1,2,4,3)])) %>%
  mutate(AES_group = factor(AES_group, labels = c("buffer areas", "cover crops", 
                                                  "maintaining grassland", "conversion to grassland"))) %>%
  group_by(AES_group, type, FarmSpec) %>%
  summarize(fraction = sum(values)/total_nr, .groups = "keep") %>%
  slice(1) %>%
  ggplot(., aes(x=as.factor(AES_group), y = fraction, fill = type)) +
  geom_bar(stat="identity", color = "black", position = position_dodge(), lwd=0.25) +
  scale_fill_manual(values=c("#ffffcc", "#a1dab4", "#41b6c4", "#2c7fb8", "#253494")) +
  scale_x_discrete(drop = FALSE, labels = c("buffer areas" = "buffer\nstrips",
                                            "cover crops" = "cover\ncrops",
                                            "maintaining grassland" = "maintaining\ngrassland", 
                                            "conversion to grassland" = "conver-\nsion to\ngrassland")) +
  scale_y_continuous(labels = scales::percent_format()) +
  facet_wrap(vars(FarmSpec)) +
  labs(x="", y = "Fraction of farms\nwith specific AES", fill = "") +
  guides(fill=guide_legend(nrow=2, byrow = TRUE)) +
  theme_bw() +
  theme(legend.position = "bottom", legend.margin=margin(0,0,0,0),
        # text = element_text(size = 15), 
        axis.text.x = element_text(size = 6),
        strip.background = element_blank())

ggsave("all_FSA.png", width = 14, height = 8, units = "cm")
```

