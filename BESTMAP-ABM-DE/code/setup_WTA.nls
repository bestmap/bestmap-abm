;-----------------------------------------------------------------------------------;
; Copyright (C) 2023 Meike Will, UFZ
;
; This file is part of model BESTMAP-ABM-DE.
;
; BESTMAP-ABM-DE is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; BESTMAP-ABM-DE is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with BESTMAP-ABM-DE. If not, see <https://www.gnu.org/licenses/>.
;-----------------------------------------------------------------------------------;

;-----------------------------------------------------------------------------------;
; load WTA specifications according to AES properties
;-----------------------------------------------------------------------------------;
; read the AES properties (defined via the interface/input) 
; combine the information in the resulting WTA, only including AES characteristics, 
; i.e. duration and administrative effort, and no information about the farm (access to advisory)
to setup-wta-specifics
  ; define AES properties based on interface/input
  setup-aes-constants
  
  ; define file paths
  let filename-wta ""
  
  ; initialize lists for effects of contract characteristics on WTA
  set p-g-wta-mean-list []
  set p-g-wta-sd-list []
  set p-g-wta-mean-advisory-list [ ]
  set p-g-wta-sd-advisory-list [ ]
  set p-g-wta-mean-duration-list []
  set p-g-wta-sd-duration-list []
  set p-g-wta-mean-admin-list []
  set p-g-wta-sd-admin-list []
  
  ; load mean WTA and standard deviation for each farmer type
  foreach p-g-farmer-type-list
  [ i ->
    ifelse i-g-cluster?
    [ set filename-wta (word "/data/polises/BESTMAP/input/WTA_" item 0 landscape item 1 landscape "_" i ".csv") ]
    [ set filename-wta (word "../data/input/WTA_" item 0 landscape item 1 landscape "_" i ".csv") ]
    
    ; load mean WTA and standard deviation
    set p-g-wta-mean-list lput item 0 csv:from-file filename-wta p-g-wta-mean-list
    set p-g-wta-sd-list lput item 1 csv:from-file filename-wta p-g-wta-sd-list
  
    ; get mean difference in WTA when advisory is available (percentage change) and its standard deviation 
    set p-g-wta-mean-advisory-list map [ x -> x / 100 ] item 6 csv:from-file filename-wta
    set p-g-wta-sd-advisory-list map [ x -> x / 100 ] item 7 csv:from-file filename-wta
  
    ; get mean difference in WTA based on specific AES characteristics for duration and its standard deviation 
    set p-g-wta-mean-duration-list r-wta-duration filename-wta 2 4
    set p-g-wta-sd-duration-list r-wta-duration filename-wta 3 5
    
    ; get mean difference in WTA based on specific AES characteristics for administrative effort and its standard deviation
    set p-g-wta-mean-admin-list r-wta-admin filename-wta 8 10
    set p-g-wta-sd-admin-list r-wta-admin filename-wta 9 11
  ]
end

;-----------------------------------------------------------------------------------;
; define AES properties based on input (interface)
;-----------------------------------------------------------------------------------;
to setup-aes-constants
  ; combine contract length input (1/5/10 years) in list
  set p-g-duration-list (list i-g-duration-buffer-strips i-g-duration-catch-crops i-g-duration-grassland i-g-duration-conversion)
  
  ; combine strings for administrative input (low/medium/high) in list
  let t-admin-string-list (list i-g-admin-buffer-strips i-g-admin-catch-crops i-g-admin-grassland i-g-admin-conversion)
  ; translate string input for bureaucracy into numerical values (0: low, 1: medium, 2: high)
  set p-g-admin-list map [ x -> (ifelse-value x = "low" [ 0 ] x = "medium" [ 1 ] [ 2 ]) ] t-admin-string-list
  
  ; combine offered payments (defined via interface/input) in list
  set p-g-offered-payment-list (list i-g-payment-buffer-strips i-g-payment-catch-crops i-g-payment-grassland i-g-payment-conversion)
end

;-----------------------------------------------------------------------------------;
; report difference in WTA based on AES characteristics for duration
;-----------------------------------------------------------------------------------;
to-report r-wta-duration [ filename item-dur1 item-dur10 ] 
  ; translate string input for contract length into numerical values (0: 1 year, 1: 5 years, 2: 10 years)
  let t-duration-list-dummy map [ x -> (ifelse-value x = 1 [ 0 ] x = 5 [ 1 ] [ 2 ]) ] p-g-duration-list
  
  ; get difference in WTA for contract length (1 or 10 years) different to baseline (5 years)
  let t-wta-duration-one item item-dur1 csv:from-file filename
  let t-wta-duration-ten item item-dur10 csv:from-file filename
  let t-wta-duration-list (list t-wta-duration-one n-values length p-g-aes-type-list [ 0 ] t-wta-duration-ten)
  ; calculate difference in WTA based on contract length
  let r-wta-duration-specific-list (map [ [ a b ] -> item a item b t-wta-duration-list ] range length p-g-aes-type-list t-duration-list-dummy)
  
  ; translate absolute change to percentage change
  set r-wta-duration-specific-list map [ x -> x / 100 ] r-wta-duration-specific-list
  
  report r-wta-duration-specific-list
end

;-----------------------------------------------------------------------------------;
; report difference in WTA based on AES characteristics for administrative effort
;-----------------------------------------------------------------------------------;
to-report r-wta-admin [ filename item-adminlow item-adminhigh ]
  ; get difference in WTA for administrative effort (low, high) different to baseline (medium)
  let t-wta-admin-low item 0 item item-adminlow csv:from-file filename
  let t-wta-admin-high item 0 item item-adminhigh csv:from-file filename
  let t-wta-admin-list (list t-wta-admin-low 0 t-wta-admin-high)
  ; calculate difference in WTA based on administrative effort
  let r-wta-admin-specific-list map [ x -> item x t-wta-admin-list ] p-g-admin-list
  
  ; translate absolute change to percentage change
  set r-wta-admin-specific-list map [ x -> x / 100 ] r-wta-admin-specific-list
  
  report r-wta-admin-specific-list
end
