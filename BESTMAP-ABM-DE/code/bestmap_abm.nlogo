;-----------------------------------------------------------------------------------;
; Copyright (C) 2023 Meike Will, UFZ
;
; This file is part of model BESTMAP-ABM-DE.
;
; BESTMAP-ABM-DE is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; BESTMAP-ABM-DE is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with BESTMAP-ABM-DE. If not, see <https://www.gnu.org/licenses/>.
;-----------------------------------------------------------------------------------;

extensions [ gis csv ]

;-----------------------------------------------------------------------------------;
; Include external files
;-----------------------------------------------------------------------------------;
__includes
[
  "setup_fields.nls"
  "setup_farmers.nls"
  "setup_WTA.nls"
  "update_world.nls"
  "check_openness.nls"
  "social_network.nls"
  "select_fields.nls"
  "deliberation.nls"
  "helper.nls"
]

;-----------------------------------------------------------------------------------;
; Global variables, variables that are commented are defined via the interface
;-----------------------------------------------------------------------------------;
; Convention for variable names:
; g - global variable
; i - variable defined via interface
; p - variables set via input files (exogenous)
; v - variables changed in the model procedures (endogenous)
; t - temporary variable
; r - reported variable (in reporter procedure)

globals
[
  ;-----------------------------------------------------------------------------------;
  ; Model settings
  ;-----------------------------------------------------------------------------------;
  ; i-g-cluster?                  ; set true if model is run on cluster (different file paths)
  ; i-g-nlrx?                     ; set true if model is run via nlrx (no manual seed via interface, csv output)
  ; i-g-model-seed                ; manually set seed when set-seed? true
  ; i-g-years                     ; number of years simulated
  ; nlrx_id                       ; transfer the nlrx experiment name, random seed and runnumber (siminputrow) between R and NetLogo
  ; eve_job_output                ; transfer the output folder assigned by the UFZ EVE cluster between R and NetLogo

  ;-----------------------------------------------------------------------------------;
  ; GIS input
  ;-----------------------------------------------------------------------------------;
  p-g-landscape-dataset-shp       ; shp input file (field-file.shp) with GIS dataset

  ;-----------------------------------------------------------------------------------;
  ; AES input
  ;-----------------------------------------------------------------------------------;
  ; lists to combine contract details for all AES as defined via interface/input
  p-g-aes-type-list               ; list with names of all AES
  p-g-duration-list               ; list with contract duration of all AES
  p-g-admin-list                  ; list with administrativ effort of all AES
  p-g-offered-payment-list        ; list with offered payment levels for all AES

  ; individual variables for AES (defined via interface/input)
  ; i-g-duration-buffer-strips    ; contract duration buffer strips
  ; i-g-duration-catch-crops      ; contract duration catch crops
  ; i-g-duration-grassland        ; contract duration maintaining grassland
  ; i-g-duration-conversion       ; contract duration conversion of arable land to grassland

  ; i-g-admin-buffer-strips       ; contract details administrative effort buffer strips
  ; i-g-admin-catch-crops         ; contract details administrative effort catch crops
  ; i-g-admin-grassland           ; contract details administrative effort maintaining grassland
  ; i-g-admin-conversion          ; contract details administrative effort conversion of arable land to grassland

  ; i-g-payment-buffer-strips     ; offered payment buffer strips
  ; i-g-payment-catch-crops       ; offered payment catch crops
  ; i-g-payment-grassland         ; offered payment maintaining grassland
  ; i-g-payment-conversion        ; offered payment conversion of arable land to grassland

  ; i-g-area-min                  ; minimum required field size for AES

  ;-----------------------------------------------------------------------------------;
  ; Discrete choice experiment input
  ;-----------------------------------------------------------------------------------;
  p-g-wta-mean-list               ; list of lists for mean WTA for different AES and farmer types
  p-g-wta-sd-list                 ; list of lists for standard deviation for different AES and farmer types
  p-g-wta-mean-advisory-list      ; list of mean difference in WTA when advisory is available
  p-g-wta-sd-advisory-list        ; list of standard deviation of WTA difference when advisory is available
  p-g-wta-mean-duration-list      ; list of mean difference in WTA based on AES characteristics for duration
  p-g-wta-sd-duration-list        ; list of standard deviation of WTA difference based on AES characteristics for duration
  p-g-wta-mean-admin-list         ; list of mean difference in WTA based on AES characteristics for administrative effort
  p-g-wta-sd-admin-list           ; list of standard deviation of WTA difference based on AES characteristics for administrative effort

  ;-----------------------------------------------------------------------------------;
  ; Farmers' attitude
  ;-----------------------------------------------------------------------------------;
  ; i-g-openness-calculation      ; approach to calculate openness to AES (distributed/aggregated)
  ; i-g-access-to-advisory        ; probability that a farmer has access to advisory
  ; i-g-social-network-type       ; choose which other farmers have an influence (spatial neighbors)
  ; i-g-social-network-radius     ; radius in which other farmers are considered if influence is from spatial neighbors
  ; i-g-prob-open-experience      ; probability that farmer with prior experience is open
  ; i-g-prob-open-social          ; probability that farmer with positive social influence is open

  ;-----------------------------------------------------------------------------------;
  ; Farmer types
  ;-----------------------------------------------------------------------------------;
  p-g-farmer-type-list            ; list of farmer types
]

;-----------------------------------------------------------------------------------;
; Breeds
;-----------------------------------------------------------------------------------;
breed [ farmers farmer ]
breed [ fields field ]
breed [ aes a-aes ]

farmers-own
[
  ;-----------------------------------------------------------------------------------;
  ; Input
  ;-----------------------------------------------------------------------------------;
  p-farmer-id 		               ; farmer ID read from input file
  p-farmer-type                  ; farmer type depending on Farming System Archetype (FSA)
  p-farm-area                    ; size of farm given by sum of field sizes
  p-property-set                 ; agentset of all fields a farmer owns
  p-advisory			               ; if a farmer has access to advisory (1/0)

  ;-----------------------------------------------------------------------------------;
  ; Openness
  ;-----------------------------------------------------------------------------------;
  v-prior-experience-list        ; list of farmer's prior experience with each AES (1/0)
  p-social-network               ; agentset of farmers that influence the farmer
  t-neighbor?                    ; helper variable for setting up the social-network
  v-open-to-aes-list             ; list of AES the farmer is open to (1/0)

  ;-----------------------------------------------------------------------------------;
  ; Willingness to accept
  ;-----------------------------------------------------------------------------------;
  v-accepted-payment-list        ; list of accepted payment level calculated based on AES contract details and farmer characteristics
  v-accepted-aes-list            ; list indicating for each AES whether offered payment level is larger or equal compared to the accepted payment level (0/1)

  ;-----------------------------------------------------------------------------------;
  ; AES fields
  ;-----------------------------------------------------------------------------------;
  v-suitable-fields-list         ; list of agentset of all fields suitable for each AES
  v-contract-area-list           ; list of total area on which farmer has AES contracts for each AES
  v-nr-aes-fields-list           ; list of number of AES contracts for each AES
  p-probability-aes              ; probability for participation in AES (Model 1 in Paulus et al., 2022)
  p-envisioned-aes-area          ; intended proportion of farm area under AES (Model 2 in Paulus et al., 2022)
  p-aes-area-scaling             ; scaling factor to overcome mismatch between predicted and actual area under AES
  v-aes-fraction-list            ; list of distribution of AES area for all AES (sum = 1)

  ;-----------------------------------------------------------------------------------;
  ; Variables for probability for AES participation & proportion of AES area
  ; (Paulus et al., 2022)
  ;-----------------------------------------------------------------------------------;
  ; all quantitative variables are scaled by their standard deviation and centered by their mean
  p-econ-int                     ; log-transformed economic intensity of farm (€/ha)
  p-EFA-farm                     ; farm has ecological focus areas (true/false)
  p-farmarea-transformed         ; log-transformed size of farm (ha)
  p-farmspec-arable              ; Farm specialization arable (true/false) (based on BESTMAP's farming system archetypes)
  p-farmspec-grassland           ; Farm specialization grassland (true/false) (based on BESTMAP's farming system archetypes)
  p-mean-fieldsize               ; log-transformed mean size of farm's fields (ha)
  p-organic                      ; organic farm (true/false)
  p-shannon-crop                 ; log-transformed Shannon Index of farm's crop diversity
  p-ANC-farm                     ; farm's area in area of natural or other specific constraints (%)
  p-elevation-farm               ; mean elevation of farm’s fields (m a.s.l.)
  p-erosion-risk-farm            ; mean erosion risk of farm’s fields (true/false)
  p-forest-edge-farm             ; farm’s area adjacent to forests (20 m buffer) (%)
  p-natura2000-farm              ; farm’s area in Natura2000 area (%)
  p-nitrate-risk-farm            ; farm’s area in nitrate risk area ( %)
  p-nr-SWF-farm                  ; number of small woody features intersecting farm’s fields
  p-soil-fertility-farm          ; median soil fertility of farm’s fields (1–5)
  p-waterbody-farm               ; farm’s area adjacent to water body (20 m buffer) (%)
  p-water-prot-area-farm         ; farm’s area in water protection area (%)
]

fields-own
[
  p-owner-id                     ; owner ID (farmer)
  p-field-id                     ; field ID
  p-land-use                     ; land use on the field
  p-area                         ; size of the field
  p-EFA-field                    ; EFA status of the field (1/0)
  p-desirability-list            ; AES specific list of factors to sort fields for AES allocation (Model 4 in Paulus et al., 2022)
  v-aes-list                     ; list of AES currently applied on the field
  v-aes-hist-list                ; list of history of AES previously applied to the field

  ;-----------------------------------------------------------------------------------;
  ; Variables for AES allocation (Paulus et al., 2022)
  ;-----------------------------------------------------------------------------------;
  ; all quantitative variables are scaled by their standard deviation and centered by their mean
  p-fieldarea-transformed        ; log-transformed size of field (ha)
  p-ANC                          ; field lies in area of natural or other specific constraints (true/false)
  p-elevation                    ; mean elevation of field (m a.s.l.)
  p-erosion-risk                 ; erosion risk of field (true/false)
  p-forest-edge                  ; forest edge within 20 m buffer from field (true/false)
  p-natura2000                   ; field lies in Natura2000 area (true/false)
  p-nitrate-risk                 ; field lies in nitrate risk area (true/false)
  p-nr-SWF                       ; number of small woody features intersecting field
  p-soil-fertility               ; median soil fertility of field (1–5)
  p-waterbody                    ; water body within 20 m buffer from field (true/false)
  p-water-prot-area              ; field lies in water protection area (true/false)
]

aes-own
[
  v-aes-nr                       ; number of AES type (0: buffer strip, 1: catch crops, 2: grassland, 3: conversion)
  v-aes-contract-year            ; current duration of AES contract (in years) 				
  v-aes-size		                 ; size of AES contract (field size)
  v-aes-owner                    ; farmer who signed contract for that AES (agentset)
  v-aes-field     	             ; field on which AES is applied (agentset)
]

;-----------------------------------------------------------------------------------;
; Setup
;-----------------------------------------------------------------------------------;
to setup
  clear-all
  reset-timer
  ; set seed - only necessary when using locally not via nlrx
  if not i-g-nlrx? [ random-seed i-g-model-seed ]

  ; create the landscape (fields) by loading GIS data (setup_fields.nls)
  setup-landscape

  ; load input file and define properties to setup farmers (setup_farmers.nls)
  setup-farmers

  ; setup social network (social_network.nls)
  setup-social-network

  ; load WTA according to AES properties (setup_WTA.nls)
  setup-wta-specifics

  reset-ticks
end

;-----------------------------------------------------------------------------------;
; Go
;-----------------------------------------------------------------------------------;
to go
  reset-timer

  ; write manual csv output and stop if simulated time span is reached
  if (ticks > i-g-years - 1)
  [
    if i-g-nlrx? [ write-list-output ]
    stop
  ]

  ; Step 0: Update prior knowledge, AES contract year and remove AES (update_world.nls)
  update-world

  ; Step 1: Derive openness to specific AES (check_openness.nls)
  check-openness-to-aes

  ; Step 2: Select fields suitable for AES (select_fields.nls)
  select-suitable-fields

  ; Step 3: Deliberation (deliberation.nls)
  ; check for each farmer and AES if the offered-payment is high enough
  ; select fields where each AES should be adopted
  make-aes-decision

  tick
end
@#$#@#$#@
GRAPHICS-WINDOW
215
10
973
769
-1
-1
5.0
1
10
1
1
1
0
1
1
1
0
149
0
149
0
0
1
ticks
30.0

BUTTON
10
115
73
148
setup
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
10
360
165
420
i-g-access-to-advisory
0.59
1
0
Number

INPUTBOX
980
620
1135
680
i-g-payment-buffer-strips
755.0
1
0
Number

INPUTBOX
1140
620
1295
680
i-g-payment-catch-crops
200.0
1
0
Number

INPUTBOX
1300
620
1455
680
i-g-payment-grassland
305.2
1
0
Number

INPUTBOX
1460
620
1615
680
i-g-payment-conversion
0.0
1
0
Number

CHOOSER
980
570
1135
615
i-g-admin-buffer-strips
i-g-admin-buffer-strips
"low" "medium" "high"
1

CHOOSER
1460
570
1615
615
i-g-admin-conversion
i-g-admin-conversion
"low" "medium" "high"
1

CHOOSER
1140
570
1295
615
i-g-admin-catch-crops
i-g-admin-catch-crops
"low" "medium" "high"
1

CHOOSER
1300
570
1455
615
i-g-admin-grassland
i-g-admin-grassland
"low" "medium" "high"
1

BUTTON
10
185
73
218
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
10
50
162
110
i-g-model-seed
4.0
1
0
Number

SWITCH
10
10
115
43
i-g-cluster?
i-g-cluster?
1
1
-1000

INPUTBOX
980
690
1135
750
i-g-area-min
0.0
1
0
Number

INPUTBOX
10
225
162
285
i-g-years
1.0
1
0
Number

BUTTON
10
150
87
183
go-once
go
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

CHOOSER
10
435
165
480
i-g-social-network-type
i-g-social-network-type
"none" "neighbors"
0

INPUTBOX
10
485
165
545
i-g-social-network-radius
5.0
1
0
Number

CHOOSER
10
300
157
345
landscape
landscape
"DE-2019" "DE-sample"
1

PLOT
985
10
1270
230
Number of AES contracts
tick
AES Contracts
0.0
1.0
0.0
120.0
true
true
"" ""
PENS
"Total" 1.0 0 -16777216 true "" "plot count aes"
"BF" 1.0 0 -12087248 true "" "plot count aes with [v-aes-nr = 0]"
"CC" 1.0 0 -5207188 true "" "plot count aes with [v-aes-nr = 1]"
"MG" 1.0 0 -11033397 true "" "plot count aes with [v-aes-nr = 2]"
"CNV" 1.0 0 -1604481 true "" "plot count aes with [v-aes-nr = 3]"

TEXTBOX
985
490
1135
508
BF: buffer strips
11
0.0
1

TEXTBOX
1150
490
1300
508
CC: catch-crops
11
0.0
1

TEXTBOX
1310
490
1460
516
MG: maintaining grassland
11
0.0
1

TEXTBOX
1470
490
1615
525
CNV: conversion of arable land to grassland
11
0.0
1

TEXTBOX
1110
465
1140
505
■
40
54.0
1

TEXTBOX
1265
465
1295
505
■
40
36.0
1

TEXTBOX
1435
465
1465
511
■
40
96.0
1

TEXTBOX
1595
465
1620
510
■
40
17.0
1

PLOT
1280
10
1590
230
Fraction of farmers with AES
NIL
NIL
0.0
1.0
0.0
0.2
true
true
"" ""
PENS
"Total" 1.0 0 -16777216 true "" "plot count farmers with [ sum v-contract-area-list > 0 ] / count farmers"
"BF" 1.0 0 -12087248 true "" "plot count farmers with [ item 0 v-contract-area-list > 0 ] / count farmers"
"CC" 1.0 0 -5207188 true "" "plot count farmers with [ item 1 v-contract-area-list > 0 ] / count farmers"
"MG" 1.0 0 -11033397 true "" "plot count farmers with [ item 2 v-contract-area-list > 0 ] / count farmers"
"CNV" 1.0 0 -1604481 true "" "plot count farmers with [ item 3 v-contract-area-list > 0 ] / count farmers"

PLOT
1280
240
1590
460
Number of fields with AES
NIL
NIL
0.0
1.0
0.0
100.0
false
true
"" ""
PENS
"Total" 1.0 0 -16777216 true "" "plot sum reduce [ [ a b ] -> (map + a b) ] [ v-aes-list] of fields\n"
"BF" 1.0 0 -12087248 true "" "plot item 0 reduce [ [ a b ] -> (map + a b) ] [ v-aes-list] of fields"
"CC" 1.0 0 -5207188 true "" "plot item 1 reduce [ [ a b ] -> (map + a b) ] [ v-aes-list] of fields"
"MG" 1.0 0 -11033397 true "" "plot item 2 reduce [ [ a b ] -> (map + a b) ] [ v-aes-list] of fields"
"CNV" 1.0 0 -1604481 true "" "plot item 3 reduce [ [ a b ] -> (map + a b) ] [ v-aes-list] of fields"

INPUTBOX
10
600
165
660
i-g-prob-open-experience
0.8
1
0
Number

INPUTBOX
10
665
165
725
i-g-prob-open-social
0.0
1
0
Number

CHOOSER
980
520
1135
565
i-g-duration-buffer-strips
i-g-duration-buffer-strips
1 5 10
1

CHOOSER
1140
520
1295
565
i-g-duration-catch-crops
i-g-duration-catch-crops
1 5 10
1

CHOOSER
1300
520
1455
565
i-g-duration-grassland
i-g-duration-grassland
1 5 10
1

CHOOSER
1460
520
1615
565
i-g-duration-conversion
i-g-duration-conversion
1 5 10
1

INPUTBOX
985
815
1090
875
nlrx_id
0
1
0
String

INPUTBOX
1100
815
1205
875
eve_job_output
0
1
0
String

TEXTBOX
990
785
1140
811
ID widgets for running on cluster via NLRX:
11
0.0
1

SWITCH
120
10
210
43
i-g-nlrx?
i-g-nlrx?
1
1
-1000

CHOOSER
10
550
167
595
i-g-openness-calculation
i-g-openness-calculation
"distributed" "aggregated"
0

@#$#@#$#@
## WHAT IS IT?

(a general understanding of what the model is trying to show or explain)

## HOW IT WORKS

(what rules the agents use to create the overall behavior of the model)

## HOW TO USE IT

(how to use the model, including a description of each of the items in the Interface tab)

## THINGS TO NOTICE

(suggested things for the user to notice while running the model)

## THINGS TO TRY

(suggested things for the user to try to do (move sliders, switches, etc.) with the model)

## EXTENDING THE MODEL

(suggested things to add or change in the Code tab to make the model more complicated, detailed, accurate, etc.)

## NETLOGO FEATURES

(interesting or unusual features of NetLogo that the model uses, particularly in the Code tab; or where workarounds were needed for missing features)

## RELATED MODELS

(models in the NetLogo Models Library and elsewhere which are of related interest)

## CREDITS AND REFERENCES

(a reference to the model's URL on the web if it has one, as well as any other necessary credits, citations, and links)
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.2.2
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
1
@#$#@#$#@
