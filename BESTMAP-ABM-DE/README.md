# BESTMAP-ABM-DE

BESTMAP-ABM-DE is an agent-based model to determine the adoption and spatial allocation of agri-environmental schemes by individual farmers in the Mulde River Basin located in Western Saxony, Germany. With the model, the effect of different scenarios of policy design on patterns of adoption can be investigated. In particular, the model can be used to study the social-ecological consequences of agricultural policies at different spatial and temporal scales and, in combination with biophysical models, test the ecological implications of different designs of the EU’s Common Agricultural Policy.

The model was developed in the [BESTMAP project](https://bestmap.eu/) as one of five case study-specific models with the same core processes.

# Input

## Landscape
Spatially explicit information on individual fields was derived from the InVeKoS database of Saxony, which is part of the EU-wide Integrated Administration and Control System (IACS). This data set contains confidential information and can thus not be made publicly available. However, the data can be requested from the Saxon State Ministry for Energy, Climate Protection, Environment and Agriculture (Sächsisches Staatsministerium für Energie, Klimaschutz, Umwelt und Landwirtschaft - SMEKUL) for research purposes. To prepare the landscape data as ABM input, the file `src\data_ABM_DE.R` has to be run. The resulting files need to be saved in the folder `data\landscape\DE-2019` or `data\landscape\DE-sample` for a sample of 50 randomly selected farmers and their fields (not in repository). 

## Parameterization
Input files for accepted payment levels and the regression results based on [Paulus et al. (2022)](https://www.sciencedirect.com/science/article/pii/S0264837722003477?via%3Dihub) are stored in the folder `data\input`. The accepted payment levels (`WTA_DE_<farm_specialization>_<economic_size>.csv`) can be derived using the file `src\calculate_WTA.R` based on input generated in `src\payment_DE.R` and `src\AES_analysis_DE.Rmd`. The Beta Regression following the approach developed in [Paulus et al. (2022)](https://www.sciencedirect.com/science/article/pii/S0264837722003477?via%3Dihub) is performed in `src\beta_regression.R`.

## Code
The NetLogo code is in the folder `code` with `bestmap_abm.nlogo` as the main file. 

# Output

## NLRX
The model can be run using the [R nlrx package](https://github.com/ropensci/nlrx). The respective files for running the analyses locally and on a cluster are in the folder `nlrx`. A simulation run creates an output files in the folder `data\output` (not in repository) with a filename in the following format: `<run_ID>_<scenario>_<case_study>_<nr_repetitions>.csv`. Local nlrx runs need a subfolder `data\output\temp` (not in repository). 

# Corresponding author: 
Meike Will (E-mail: <meike.will@ufz.de>)

Please get in touch if you have questions about the code or data.